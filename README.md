# play-cnz

Practical extensions for Play Framework (Scala).

This module currently includes the following.

- Implementations of query string binders for filtering, pagination, and sorting.
- Model classes to facilitate the use of Anorm.
- A class for the Postgres ltree data type with implicits for Anorm.
- A JSON web token implementation including a Play Framework action for verifying and extracting the JWT payload.
- Classes for [JSON:API](https://jsonapi.org/).
- Clients for Linode Object Storage and AWS S3.

See the [API documentation](https://www.cnz.com/doc/api/com/cnz/play/) for details.

## Building

1. Clone this repository.
1. cd into the base directory of the cloned repository
1. Run `sbt`.
1. In the sbt shell, run `package`, then `scalastyle`, then `test`, then `doc`, and then `publishLocal`.
1. To generate a code coverage report, run `clean`, then `coverage`, then `test`, and then `coverageReport`.  The last command will show the filesystem path for the HTML coverage report.
1. Exit sbt with `exit`.

The coverage report will be available at the following URL (replace /path/to/play-cnz to the directory containing your clone of play-cnz).

`file:///path/to/play-cnz/target/scala-2.13/scoverage-report/index.html`

To use play-cnz, add the following library dependency to your Play application.

```
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
libraryDependencies += "com.cnz.play" %% "play-cnz" % "0.2.4-SNAPSHOT"
```

Then either add the following to the application configuration or set the environment variable `JWT_SECRET`.

```
cnz.jwt.secret = "a-secret-string"
```

For more details, see the [example application](https://gitlab.com/lfrost/play-cnz-example).
