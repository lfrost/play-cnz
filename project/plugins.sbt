ThisBuild / libraryDependencySchemes += "org.scala-lang.modules" %% "scala-xml" % VersionScheme.Always

addSbtPlugin("com.beautiful-scala" % "sbt-scalastyle" % "1.5.1")
addSbtPlugin("com.github.sbt"      % "sbt-git"        % "2.0.1")
addSbtPlugin("com.typesafe.play"   % "sbt-plugin"     % "2.8.20")
addSbtPlugin("org.scoverage"       % "sbt-scoverage"  % "2.0.8")
