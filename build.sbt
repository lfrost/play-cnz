/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

ThisBuild / organization  := "com.cnz.play"
ThisBuild / scalaVersion  := "2.13.11"
ThisBuild / version       := "0.3.1-SNAPSHOT"
ThisBuild / versionScheme := Some("early-semver")

ThisBuild / libraryDependencySchemes += "io.circe" %% "circe-core" % "early-semver"

val gitBaseUrl = settingKey[String]("Git base URL")
gitBaseUrl := "https://gitlab.com/lfrost"

lazy val root = (project in file(".")).
    settings(
        name := "play-cnz",
        libraryDependencies ++= Seq(
            jdbc,
            "com.h2database"          %  "h2"                   % "2.2.222",
            "com.mysql"               %  "mysql-connector-j"    % "8.1.0",
            "de.svenkubiak"           %  "jBCrypt"              % "0.4.3",
            "org.postgresql"          %  "postgresql"           % "42.6.0",
            "software.amazon.awssdk"  %  "s3"                   % "2.20.140",
            "com.typesafe.play"       %% "play-json"            % "2.9.4",
            "org.playframework.anorm" %% "anorm"                % "2.7.0",
            "org.playframework.anorm" %% "anorm-postgres"       % "2.7.0",
            "org.scalamock"           %% "scalamock"            % "5.2.0"    % Test,
            "org.scalatestplus.play"  %% "scalatestplus-play"   % "5.1.0"    % Test
        ),
        // https://docs.scala-lang.org/overviews/compiler-options/
        scalacOptions ++= Seq(
            // Standard Settings
            "--deprecation",            // Emit warning and location for usages of deprecated APIs.
            "--encoding", "UTF-8",      // Specify character encoding used by source files.
            "--explain-types",          // Explain type errors in more detail.
            "--feature",                // Emit warning and location for usages of features that should be imported explicitly.
            "--release:11",             // Target platform for object files. (8,9,10,11,12,13,14,15,16,[17])
            "--unchecked",              // Enable additional warnings where generated code depends on assumptions.

            // Advanced Settings

            // Warning Settings
            "-Ywarn-dead-code",         // Warn when dead code is identified.
            "-Ywarn-value-discard",     // Warn when non-Unit expression results are unused.
            "-Ywarn-numeric-widen",     // Warn when numerics are widened.
            "-Ywarn-unused:_",          // Enable or disable specific unused warnings: _ for all, -Ywarn-unused:help to list choices.
            "-Ywarn-extra-implicit",    // Warn when more than one implicit parameter section is defined.
            "-Xlint:_"                  // Enable or disable specific warnings: _ for all, -Xlint:help to list choices.
        ),
        apiURL := Some(url("https://www.cnz.com/doc/api/")),
        Compile / packageSrc / publishArtifact := false,
        Compile / doc / scalacOptions ++=
            Opts.doc.title("CNZ Module for Play")                                                                                  ++
            Opts.doc.sourceUrl(gitBaseUrl.value + "/" + name.value + "/tree/" + git.gitCurrentBranch.value + "€{FILE_PATH}.scala") ++
            Seq("-sourcepath", baseDirectory.value.getAbsolutePath),
        Test / parallelExecution := false,
        publishTo := {
            val nexus = "https://oss.sonatype.org/"
            if (isSnapshot.value)
                Some("snapshots" at nexus + "content/repositories/snapshots")
            else
                Some("releases"  at nexus + "service/local/staging/deploy/maven2")
        },
        credentials += Credentials(Path.userHome / ".sbt" / "credentials" / "oss.sonatype.org-com.cnz"),
        exportJars := true,
        publishMavenStyle := true
    )
