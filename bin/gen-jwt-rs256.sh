#!/bin/bash -fu
# Copyright © 2017-2021 Lyle Frost <lfrost@cnz.com>
#
# Generate an RS256 key pair for signing JWTs.
#
# This has only been tested with Bash 5 on Debian 10.
#
# openssl must be installed before using this script.
#

declare -ir EXIT_SUCCESS=0
declare -ir EXIT_FAILURE=1
declare -ir EXIT_USAGE=2

if ! set -o pipefail
then
    echo 'Error setting shell options.'
    exit $EXIT_FAILURE
fi

declare -r SCRIPT_NAME="${0##*/}"
declare -r USAGE_MSG="Usage: $SCRIPT_NAME -n name"

declare -r CERT_DIR="$HOME/.cert"

# Process the command line.
declare name=''
while getopts ':n:' opt
do
    case "$opt" in
    n )
        name="$OPTARG"
        ;;
    ? )
        echo "$USAGE_MSG"
        exit $EXIT_USAGE
        ;;
    esac
done
shift $(($OPTIND - 1))
if (($# != 0))
then
    echo "$USAGE_MSG"
    exit $EXIT_USAGE
fi
if [ -z "$name" ]
then
    echo "$USAGE_MSG"
    echo 'Missing name option.'
    exit $EXIT_USAGE
fi

# Create certificate directory.
if ! [ -d "$CERT_DIR" ]
then
    if ! mkdir --mode 0700 "$CERT_DIR"
    then
        echo "Error creating certificate directory $CERT_DIR."
        exit $EXIT_FAILURE
    fi
fi
declare -r CERT_SUB_DIR="$CERT_DIR/$name"
if [ -d "$CERT_SUB_DIR" ]
then
    echo "Certificate sub-directory $CERT_SUB_DIR already exists."
    exit $EXIT_FAILURE
fi
if ! mkdir --mode 0700 "$CERT_SUB_DIR"
then
    echo "Error creating certificate sub-directory $CERT_SUB_DIR."
    exit $EXIT_FAILURE
fi

declare -r PRIVATE_KEY="$CERT_SUB_DIR/rs256.pkcs8.pem"
declare -r PUBLIC_KEY="$CERT_SUB_DIR/rs256.pub.pem"

if ! openssl genrsa | openssl pkcs8 -topk8 -inform pem -outform pem -nocrypt -out "$PRIVATE_KEY"
then
    echo 'Error generating PKCS#8 key.'
    exit $EXIT_FAILURE
fi

if ! openssl rsa -in "$PRIVATE_KEY" -pubout > "$PUBLIC_KEY"
then
    echo 'Error extracting public key.'
    exit $EXIT_FAILURE
fi

echo "New JWT RS256 private key is in $PRIVATE_KEY."

exit $EXIT_SUCCESS
