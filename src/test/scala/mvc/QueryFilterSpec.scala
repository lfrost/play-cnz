/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import com.cnz.play.mvc.QueryFilter

/** QueryFilter tests
 */
class QueryFilterSpec extends QueryBaseSpec {
    val aKey         = "filter"
    val errorMessage = "Unable to bind filter to a QueryFilter."

    "QueryFilter constructor with invalid field name" should "throw IllegalArgumentException" in {
        assertThrows[IllegalArgumentException] {
            QueryFilter(filter = Map("" -> Seq("foobar")))
        }
    }

    "Query string with valid filter" should "bind successfully" in {
        val result = QueryFilter.queryStringBindable.bind(aKey, Map(s"$aKey[Person.nameLast]" -> Seq("Howard"), s"$aKey[Person.nameFirst]" -> Seq("Robert"), s"$aKey[Person.nameMiddle]" -> Seq("E.")))
        result     shouldBe defined
        result.get shouldBe (Right(QueryFilter(filter = Map("Person.nameLast" -> Seq("Howard"), "Person.nameFirst" -> Seq("Robert"), "Person.nameMiddle" -> Seq("E.")))))
    }

    "Empty query string" should "bind as None" in {
        QueryFilter.queryStringBindable.bind(aKey, Map.empty[String, Seq[String]]) shouldBe None
    }

    "Query string with no filter" should "bind as None" in {
        QueryFilter.queryStringBindable.bind(aKey, Map("foo" -> Seq("bar"))) shouldBe None
    }

    "Empty QueryFilter object" should "unbind correctly" in {
        val qf = QueryFilter(filter = Map.empty[String,Seq[String]])
        QueryFilter.queryStringBindable.unbind(aKey, qf) shouldBe ("")
    }

    "Query string with invalid field name" should "ignore invalid field name" in {
        val result = QueryFilter.queryStringBindable.bind(aKey, Map(s"$aKey[]" -> Seq("Howard"), s"$aKey[Person.nameFirst]" -> Seq("Robert"), s"$aKey[Person.nameMiddle]" -> Seq("E.")))
        result     shouldBe defined
        result.get shouldBe (Right(QueryFilter(filter = Map("Person.nameFirst" -> Seq("Robert"), "Person.nameMiddle" -> Seq("E.")))))
    }

    "QueryFilter object with a single filter" should "unbind correctly" in {
        val qf = QueryFilter(filter = Map("foo!" -> Seq("bar!")))
        QueryFilter.queryStringBindable.unbind(aKey, qf) shouldBe (enc(s"$aKey[foo!]") + "=" + enc("bar!"))
    }

    "QueryFilter object with a multiple filters" should "unbind correctly" in {
        val qf = QueryFilter(filter = Map("Person.nameLast" -> Seq("Howard"), "Person.nameFirst" -> Seq("Robert")))
        QueryFilter.queryStringBindable.unbind(aKey, qf) shouldBe (enc(s"$aKey[Person.nameLast]") + "=Howard&" + enc(s"$aKey[Person.nameFirst]") + "=Robert")
    }

    "QueryFilter object with a multiple filters with multiple values" should "unbind correctly" in {
        val qf = QueryFilter(filter = Map("Person.nameLast" -> Seq("Frost", "Howard"), "Person.nameFirst" -> Seq("Robert")))
        QueryFilter.queryStringBindable.unbind(aKey, qf) shouldBe (enc(s"$aKey[Person.nameLast]") + "=Frost&" + enc(s"$aKey[Person.nameLast]") + "=Howard&" + enc(s"$aKey[Person.nameFirst]") + "=Robert")
    }
}
