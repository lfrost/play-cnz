/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import com.cnz.play.mvc.QuerySort

/** QuerySort tests
 */
class QuerySortSpec extends QueryBaseSpec {
    val aKey         = "sort"
    val errorMessage = "Unable to bind sort to a QuerySort."
    val reverse      = '-'

    "Query string with valid sort" should "bind successfully" in {
        val result = QuerySort.queryStringBindable.bind(aKey, Map(aKey -> Seq("Person.nameLast,Person.nameFirst,Person.nameMiddle")))
        result     shouldBe defined
        result.get shouldBe (Right(QuerySort(sort = Seq(("Person.nameLast", false), ("Person.nameFirst", false), ("Person.nameMiddle", false)))))
    }

    "Query string with one field empty" should "fail to bind" in {
        val result = QuerySort.queryStringBindable.bind(aKey, Map(aKey -> Seq("Person.nameLast,,Person.nameMiddle")))
        result     shouldBe defined
        result.get shouldBe (Left(errorMessage))
    }

    "Query string with multiple sorts" should "bind successfully and use first sort" in {
        val result = QuerySort.queryStringBindable.bind(aKey, Map(aKey -> Seq("Person.nameLast", "Person.city")))
        result     shouldBe defined
        result.get shouldBe (Right(QuerySort(sort = Seq(("Person.nameLast", false)))))
    }

    "Query string with first field reverse" should "bind successfully" in {
        val result = QuerySort.queryStringBindable.bind(aKey, Map(aKey -> Seq("-Person.nameLast,Person.city")))
        result     shouldBe defined
        result.get shouldBe (Right(QuerySort(sort = Seq(("Person.nameLast", true), ("Person.city", false)))))
    }

    "Query string with reverse after first field" should "bind successfully" in {
        val result = QuerySort.queryStringBindable.bind(aKey, Map(aKey -> Seq("Person.nameLast,-Person.city")))
        result     shouldBe defined
        result.get shouldBe (Right(QuerySort(sort = Seq(("Person.nameLast", false), ("Person.city", true)))))
    }

    "Empty query string" should "bind as None" in {
        QuerySort.queryStringBindable.bind(aKey, Map.empty[String, Seq[String]]) shouldBe None
    }

    "Query string with no sort" should "bind as None" in {
        QuerySort.queryStringBindable.bind(aKey, Map("foo" -> Seq("bar"))) shouldBe None
    }

    "Empty QuerySort object" should "unbind correctly" in {
        val qs = QuerySort(sort = Nil)
        QuerySort.queryStringBindable.unbind(aKey, qs) shouldBe (s"$aKey=")
    }

    "QuerySort object with a single sort" should "unbind correctly" in {
        val qs = QuerySort(sort = Seq("Person.nameLast!" -> false))
        QuerySort.queryStringBindable.unbind(aKey, qs) shouldBe (aKey + "=" + enc("Person.nameLast!"))
    }

    "QuerySort object with multiple sorts" should "unbind correctly" in {
        val qs = QuerySort(sort = Seq("Person.nameLast" -> false, "Person.nameFirst" -> false))
        QuerySort.queryStringBindable.unbind(aKey, qs) shouldBe (s"$aKey=${enc("Person.nameLast,Person.nameFirst")}")
    }

    "QuerySort object with multiple negative sorts" should "unbind correctly" in {
        val qs = QuerySort(sort = Seq("Person.nameLast" -> true, "Person.nameFirst" -> true))
        QuerySort.queryStringBindable.unbind(aKey, qs) shouldBe (s"$aKey=${enc(s"${reverse}Person.nameLast,${reverse}Person.nameFirst")}")
    }

    "QuerySort object with a negative sort followed by a positive sorts" should "unbind correctly" in {
        val qs = QuerySort(sort = Seq("Person.nameLast" -> true, "Person.nameFirst" -> false))
        QuerySort.queryStringBindable.unbind(aKey, qs) shouldBe (s"$aKey=${enc(s"${reverse}Person.nameLast,Person.nameFirst")}")
    }

    "QuerySort object with a positive sort followed by a negative sort" should "unbind correctly" in {
        val qs = QuerySort(sort = Seq("Person.nameLast" -> false, "Person.nameFirst" -> true))
        QuerySort.queryStringBindable.unbind(aKey, qs) shouldBe (s"$aKey=${enc(s"Person.nameLast,${reverse}Person.nameFirst")}")
    }
}
