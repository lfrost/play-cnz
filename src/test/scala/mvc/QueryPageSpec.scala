/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import com.cnz.play.mvc.QueryPage

/** QueryPage tests
 */
class QueryPageSpec extends QueryBaseSpec {
    val aKey         = "page"
    val numberKey    = aKey + "[number]"
    val sizeKey      = aKey + "[size]"
    val invalidKey   = aKey + "[invalid]"
    val errorMessage = "Unable to bind page to a QueryPage."

    "Query string with valid page" should "bind successfully" in {
        val result = QueryPage.queryStringBindable.bind(aKey, Map(numberKey -> Seq("1"), sizeKey -> Seq("10")))
        result     shouldBe defined
        result.get shouldBe (Right(QueryPage(number = 1L, size = 10)))
    }

    "Query string with negative page number" should "fail to bind" in {
        val result = QueryPage.queryStringBindable.bind(aKey, Map(numberKey -> Seq("-1"), sizeKey -> Seq("10")))
        result     shouldBe defined
        result.get shouldBe (Left(errorMessage))
    }

    "Query string with negative page size" should "fail to bind" in {
        val result = QueryPage.queryStringBindable.bind(aKey, Map(numberKey -> Seq("1"), sizeKey -> Seq("-10")))
        result     shouldBe defined
        result.get shouldBe (Left(errorMessage))
    }

    "Query string with non-numeric page number" should "fail to bind" in {
        val result = QueryPage.queryStringBindable.bind(aKey, Map(numberKey -> Seq("foo"), sizeKey -> Seq("10")))
        result     shouldBe defined
        result.get shouldBe (Left(errorMessage))
    }

    "Query string with non-numeric page size" should "fail to bind" in {
        val result = QueryPage.queryStringBindable.bind(aKey, Map(numberKey -> Seq("1"), sizeKey -> Seq("foo")))
        result     shouldBe defined
        result.get shouldBe (Left(errorMessage))
    }

    "Query string with multiple page numbers" should "bind successfully and use first page number" in {
        val result = QueryPage.queryStringBindable.bind(aKey, Map(numberKey -> Seq("1", "2"), sizeKey -> Seq("10")))
        result     shouldBe defined
        result.get shouldBe (Right(QueryPage(number = 1L, size = 10)))
    }

    "Query string with multiple page sizes" should "bind successfully and use first page size" in {
        val result = QueryPage.queryStringBindable.bind(aKey, Map(numberKey -> Seq("1"), sizeKey -> Seq("10", "11")))
        result     shouldBe defined
        result.get shouldBe (Right(QueryPage(number = 1L, size = 10)))
    }

    "Query string with extra, undefined page index" should "bind successfully and ignore undefined page index" in {
        val result = QueryPage.queryStringBindable.bind(aKey, Map(numberKey -> Seq("1"), sizeKey -> Seq("10", "11"), invalidKey -> Seq("1")))
        result     shouldBe defined
        result.get shouldBe (Right(QueryPage(number = 1L, size = 10)))
    }

    "Empty query string" should "bind as None" in {
        QueryPage.queryStringBindable.bind(aKey, Map.empty[String, Seq[String]]) shouldBe None
    }

    "Query string with no page" should "bind as None" in {
        QueryPage.queryStringBindable.bind(aKey, Map("foo" -> Seq("bar"))) shouldBe None
    }

    "Query string with page number but not size" should "bind as None" in {
        QueryPage.queryStringBindable.bind(aKey, Map(numberKey -> Seq("1"))) shouldBe None
    }

    "Query string with page size but not number" should "bind as None" in {
        QueryPage.queryStringBindable.bind(aKey, Map(sizeKey -> Seq("1"))) shouldBe None
    }

    "QueryPage object" should "unbind correctly" in {
        val qp = QueryPage(number = 1L, size = 10)
        QueryPage.queryStringBindable.unbind(aKey, qp) shouldBe (enc(numberKey) + "=1&" + enc(sizeKey) + "=10")
    }

    "QueryPage 1"  should "be first" in {
        val qp = QueryPage(number = 1L, size = 10)
        qp.isFirst shouldBe (true)
    }

    "QueryPage 1" should "not have a prev" in {
        val qp = QueryPage(number = 1L, size = 10)
        qp.prev shouldBe None
    }

    "QueryPage 5" should "have a prev of 4" in {
        val qp = QueryPage(number = 5L, size = 10)
        qp.prev shouldBe Some(QueryPage(number = 4L, size = 10))
    }

    "QueryPage 2" should "have a first of 1" in {
        val qp = QueryPage(number = 2L, size = 10)
        qp.first shouldBe QueryPage(number = 1L, size = 10)
    }

    "QueryPage 2" should "have a next of 3" in {
        val qp = QueryPage(number = 2, size = 10)
        qp.next shouldBe QueryPage(number = 3L, size = 10)
    }

    "QueryPage 2 with page count 4" should "have a next of 3" in {
        val qp = QueryPage(number = 2L, size = 10)
        qp.next(4L) shouldBe Some(QueryPage(number = 3L, size = 10))
    }

    "QueryPage 2 with page count 2" should "have no next" in {
        val qp = QueryPage(number = 2L, size = 10)
        qp.next(2L) shouldBe None
    }

    "QueryPage 2 with page count 4" should "have a last of 4" in {
        val qp = QueryPage(number = 2L, size = 10)
        qp.last(4L) shouldBe Some(QueryPage(number = 4L, size = 10))
    }

    "QueryPage 2 with page count 0" should "have no last" in {
        val qp = QueryPage(number = 2L, size = 10)
        qp.last(0L) shouldBe None
    }
}
