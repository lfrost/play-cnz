/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import java.net.{URI, URL}

import com.cnz.play.mvc.JsonUtils.{UriReads, UriWrites, UrlReads, UrlWrites}
import org.scalatest.TryValues
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import play.api.libs.json.{JsError, Json}

/** JSON utilities tests
 */
class JsonUtilsSpec extends AnyWordSpecLike with Matchers with TryValues {
    "JSON utils" should {
        "successfully serialize a valid URI" in {
            val uriString = "news:comp.lang.scala"
            val uri       = new URI(uriString)
            val uriJson   = Json.toJson(uri)

            uriJson.as[String] shouldBe (uriString)
        }
        "successfully deserialize a valid URI" in {
            val uriString = "news:comp.lang.scala"
            val jo = Json.obj(
                "uri" -> uriString
            )
            val uri = (jo \ "uri").as[URI]
            uri.toString() shouldBe (uriString)
        }
        "fail to deserialize an invalid URI" in {
            val uriString = ":news:comp.lang.scala"
            val jo = Json.obj(
                "uri" -> uriString
            )
            val uri = (jo \ "uri").validate[URI]
            uri shouldBe (JsError("Invalid URI"))
        }
        "successfully serialize a valid URL" in {
            val urlString = "https://www.cnz.com/api"
            val url       = new URL(urlString)
            val urlJson   = Json.toJson(url)

            urlJson.as[String] shouldBe (urlString)
        }
        "successfully deserialize a valid URL" in {
            val urlString = "https://www.cnz.com/api"
            val jo = Json.obj(
                "url" -> urlString
            )
            val url = (jo \ "url").as[URL]
            url.toString() shouldBe (urlString)
        }
        "fail to deserialize an invalid URL" in {
            val urlString = ":https://www.cnz.com/api"
            val jo = Json.obj(
                "url" -> urlString
            )
            val url = (jo \ "url").validate[URL]
            url shouldBe (JsError("Invalid URL"))
        }
    }
}
