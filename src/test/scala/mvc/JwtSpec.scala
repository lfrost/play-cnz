/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import com.cnz.play.mvc.{Jwt, JwtConfig, JwtHeader, JwtPayload}
import org.scalatest.TryValues
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

/** JWT tests
 */
class JwtSpec extends AnyWordSpecLike with Matchers with TryValues {
    /** A self-signed private key used solely for tests. */
    val pem = """-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDkgdRtQP9UQwfT
iEsfDPFPp77v5ERUuwKexh81kwK5i+8RV3irrNYWCaEmiV3ONTbqRvJaEiy2WueH
pT/nsuyI9pN5hfEU+6P43zpZTBdLQE0Y1UAgTjyuI2svqTOulla6MeTr7oJ5aTop
nryNOKL52InogNgdAsggNZ/kcojrc5rAQdn0jiibHIn/M9uKwUfMr2X3qtxIIVCb
5Iv9dXXTSN6jDhJGFnU2lOGV+KxXL7k3npSNz6L6ZMvuc58rJZ1S4drdAWn+WBlK
CPDiCm1cNYsuZnOk0fUcUgsQPCwm0WbnQGapq90r9kNZLn6wclw1gOiW8U5CASa0
A/c2KlMlAgMBAAECggEAI8CkiguXZ4mtWc2DY3JU90+ZslMW1eMEv+7jxzAfRwS7
/QyglsvOG1iaXDs18COnWfSLysf5Mvb6LuEOJKR8GdnqrSLl25Cb0T6sFHixVFbV
OUN0axmSqPRluulQasqe+zb/SGLjCzgJn6V0fuwTo9/bhANUzQU4ZsG9KLM69Quv
waVWN1qFkFHKx901f4mg8/g5jyKekZL1TdxWO/DmghC71dhtXKGIssGK1mqfBFPM
GNqL1zWxJUGKprylnJkDyTqUbJbxvcOa+xe8zdI7Big4q4cdgEuz7nvtDD2AH6HG
isRFGA7DkJX1iFKEbDra0aZAtx0TVsqsSybj7Kn3aQKBgQD2m8Pffuh0q5VL6CP+
Vd33QgFPC8eNh42Z+DXgXeX1V9pKjaT3lMpYIkAtgWL95mCMPyDhf+n4oSQHb3Ft
kfMshsbjgg4l0ZLBDSANPRi+R2M3oNdoM+/ega9vdatahpbpbAgcU5wMVlB9UTx8
AxPtH+W639Qh78Ao0kadik/+swKBgQDtNZdlpVBL9O/fOna9SBViRwsGNdUVoCIY
yOPx+FxcTrZIbC9z8a/YEQeE5VzfjMAvgD1FKR8gw1c9OV8O3WNdN8yIOlxQfYbc
iiLc0I7/aWy+nOSm5KUQ6DuPd1minKLLD8vkXg3tgvj09bgSL/VZ82BMyeS9LTRI
L0ELFwJSxwKBgQDYcfSkB+4c4LBj9DcLNvBf4VXmvA8EwHHfLsSSzbdU9CvK/bFK
ABTs7/rmNcbd1ELvNUU1xqBjsYM+52hl5BDMBusvqemi9eNZf5yfIf3CnEOQAqKt
edBRuf9x05R3GuP/sesYeaXgCi4RH6j3q3fkyvdGYsf8lUJSUeTaxxfZEwKBgBXX
8IggsAM+m/Qvew21HJr/NcmtnWCvbVRg83StzTuG2AxVQg1oPSwlbU7UnDfNy0jU
g0/iGIa41kjCBrnksyi7Ya9NyIB0/r1CTzGMVw64E5Z99iRUO8sRx6JPeEacK/WN
D9aIvrmXbPuJwRiTqAFPgxevI8wz5HOdPl0aElGzAoGAMWUap3aO3Jei6lMUInVF
/9xI93Gf3VxTaGonU/5NOK6RjGhNtuK9ePrxzvUlicOZ2cx/Fu1OLWa4/YXEjuyk
JkUj9pGNLX6YUt8mSeFYu+eOMD7s+kOarE4YITjcz/jNUPNBqLe+SuaslL5J+Xaq
H7LxWlGb70v/XheJRjAXLgo=
-----END PRIVATE KEY-----"""

    "JWT configuration" should {
        "create successfully with alg none" in {
            val result = JwtConfig("none", "JWT", "")
            result.algorithm shouldBe ("none")
            result.`type`    shouldBe ("JWT")
            result.secret    shouldBe ("")
        }
        "create successfully with alg HS256" in {
            val result = JwtConfig("HS256", "JWT", "secret")
            result.algorithm shouldBe ("HS256")
            result.`type`    shouldBe ("JWT")
            result.secret    shouldBe ("secret")
        }
        "throw IllegalArgumentException with alg HS256 and no secret" in {
            the[IllegalArgumentException] thrownBy {
                JwtConfig("HS256", "JWT", "")
            } should have message "requirement failed: Invalid secret"
        }
        "throw IllegalArgumentException with alg none and a secret" in {
            the[IllegalArgumentException] thrownBy {
                JwtConfig("none", "JWT", "secret")
            } should have message "requirement failed: Invalid secret"
        }
        "throw IllegalArgumentException with unknown alg" in {
            the[IllegalArgumentException] thrownBy {
                JwtConfig("foo", "JWT", "secret")
            } should have message "requirement failed: Invalid algorithm"
        }
        "throw IllegalArgumentException with unknown token type" in {
            the[IllegalArgumentException] thrownBy {
                JwtConfig("HS256", "JTW", "secret")
            } should have message "requirement failed: Invalid type"
        }
    }

    "JWT header" should {
        "create successfully with alg none" in {
            val result = JwtHeader(alg = "none", typ = "JWT")
            result.alg shouldBe ("none")
            result.typ shouldBe ("JWT")
        }
        "create successfully with alg HS256" in {
            val result = JwtHeader(alg = "HS256", typ = "JWT")
            result.alg shouldBe ("HS256")
            result.typ shouldBe ("JWT")
        }
        "throw IllegalArgumentException with unknown alg" in {
            the[IllegalArgumentException] thrownBy {
                JwtHeader(alg = "foo", typ = "JWT")
            } should have message "requirement failed: Invalid algorithm"
        }
        "throw IllegalArgumentException with unknown token type" in {
            the[IllegalArgumentException] thrownBy {
                JwtHeader(alg = "HS256", typ = "JTW")
            } should have message "requirement failed: Invalid type"
        }
    }

    "JWT" should {
        "sign successfully with alg RS256" in {
            val jwtHeader = JwtHeader(alg = "RS256", typ = "JWT")
            val jwtPayload = JwtPayload(iss = Some("Issuer"), sub = Some("Subject"))
            val jwt = Jwt(jwtHeader = jwtHeader, jwtPayload = jwtPayload)
            implicit val jwtConfig = JwtConfig("RS256", "JWT", pem)
            val result = jwt.sign()
            result.isSuccess shouldBe (true)
        }
    }
}
