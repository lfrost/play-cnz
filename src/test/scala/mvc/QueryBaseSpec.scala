/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import java.nio.charset.StandardCharsets.UTF_8

import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/** Base class for HTTP query tests
 */
abstract class QueryBaseSpec extends AnyFlatSpec with EitherValues with Matchers {
    /** The query string key name
     */
    val aKey:String

    /** The expected bind error message
     */
    val errorMessage:String

    /** URL encode a string with charset UTF-8.
     *
     *  @param s The string to encode.
     *  @return  s encoded for URLs
     */
    def enc(s:String) : String = java.net.URLEncoder.encode(s, UTF_8.name)
}
