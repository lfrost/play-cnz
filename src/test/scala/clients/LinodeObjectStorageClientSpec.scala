/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import java.util.concurrent.CompletableFuture

import scala.jdk.CollectionConverters._     // scalastyle:ignore underscore.import

import com.cnz.play.clients.LinodeObjectStorageClient
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AsyncWordSpec
import play.api.Configuration
import software.amazon.awssdk.services.s3.S3AsyncClient
import software.amazon.awssdk.services.s3.model.{Bucket, ListBucketsRequest, ListBucketsResponse}

/** LinodeObjectStorageClient tests
 */
class LinodeObjectStorageClientSpec extends AsyncWordSpec with AsyncMockFactory with Matchers {
    val mockConfigurationData:Map[String, Any] = Map(
        "linode" -> Map(
            "region" -> "us-east",
            "credentials" -> Map(
                "accessKey" -> "foo",
                "secretKey" -> "bar"
            )
        )
    )
    val mockConfiguration = Configuration.from(mockConfigurationData)

    "A LinodeObjectStorageClient" must {
        val mockS3 = mock[S3AsyncClient]

        "list buckets" in {
            val bucket         = Bucket.builder.name("test-bucket").build()
            val request        = ListBucketsRequest.builder.build()
            val response       = ListBucketsResponse.builder.buckets(bucket).build()
            val expectedResult = response.buckets.asScala.toSeq

            (mockS3.listBuckets(_:ListBucketsRequest)).expects(request).returning(CompletableFuture.completedFuture(response))

            val losc = new LinodeObjectStorageClient(mockConfiguration) {
                override lazy val client = mockS3
            }
            val futureResult = losc.listBuckets()
            futureResult map { result => result shouldBe expectedResult }
        }
    }
}
