/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import com.cnz.play.configs.LinodeConfig
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import play.api.Configuration

/** LinodeConfig tests
 */
class LinodeConfigSpec extends AnyWordSpec with Matchers {
    "A LinodeConfig" must {
        "fail to create for bad configuration" in {
            val mockBadConfigurationData:Map[String, Any] = Map(
                "linode" -> Map(
                    "region" -> "us-east",
                    "credentials" -> Map(
                        "secretKey" -> "bar"
                    )
                )
            )
            val mockBadConfiguration = Configuration.from(mockBadConfigurationData)

            assertThrows[com.typesafe.config.ConfigException$Missing] {
                mockBadConfiguration.get[LinodeConfig]("linode")
            }
        }
    }
}
