/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import java.sql.{Connection => DbConnection}
import java.util.UUID

import scala.util.Try

import anorm.{~, NamedParameter, ResultSetParser}
import anorm.SqlParser.{get, scalar}
import com.cnz.play.anorm.{ModelH2, ModelH2Companion}
import com.cnz.play.mvc.{QueryFilter, QueryPage, QuerySort}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/** Model tests
 */
class ModelSpec extends AnyFlatSpec with Matchers {
    case class TestModel(
        id           : Option[UUID] = None,
        stringColumn : String,
        optIntColumn : Option[Int]  = None
    ) extends ModelH2[TestModel, UUID] {
        override def companion : ModelH2Companion[TestModel, UUID] = TestModel
    }
    object TestModel extends ModelH2Companion[TestModel, UUID] {
        override val Table = "Test"
        override val PrimaryKeyResultSetParser : ResultSetParser[UUID] = scalar[UUID].single
        override val InsertColumns = Seq("stringColumn", "optIntColumn")
        override def insertValues(testModel:TestModel) : Seq[NamedParameter] = {
            Seq(
                "stringColumn" -> testModel.stringColumn,
                "optIntColumn" -> testModel.optIntColumn
            )
        }
        override val RowParser = {
            get[Option[UUID]]("TestModel.id")          ~
            get[String]("TestModel.stringColumn")      ~
            get[Option[Int]]("TestModel.optIntColumn") map {
                case id ~ stringColumn ~ optIntColumn =>
                TestModel(id = id, stringColumn = stringColumn, optIntColumn = optIntColumn)
            }
        }
        override val RowParserColumns =
            """|    "TestModel"."id",
            |    "TestModel"."stringColumn",
            |    "TestModel"."optIntColumn""""
        override def insert(testModel:TestModel)(implicit dbConnection:DbConnection) : Try[UUID] = {
            doInsert(PrimaryKeyResultSetParser,
                "stringColumn" -> testModel.stringColumn,
                "optIntColumn" -> testModel.optIntColumn
            )
        }
    }

    "TestModel insert columns SQL" should "be \"stringColumn\", \"optIntColumn\"" in {
        TestModel.sqlInsertColumns(TestModel.InsertColumns) shouldBe ("\"stringColumn\", \"optIntColumn\"")
    }
    "TestModel SQL parameter placeholder for TestModel.stringColumn" should "be {\"stringColumn\", \"optIntColumn\"}" in {
        TestModel.sqlParameterPlaceholder("TestModel.stringColumn") shouldBe ("{TestModel.stringColumn}")
    }
    "TestModel SQL parameter placeholder for Test\"Model.string\"Column" should "be {\"stringColumn\", \"optIntColumn\"}" in {
        TestModel.sqlParameterPlaceholder("Test\"Model.string\"Column") shouldBe ("{TestModel.stringColumn}")
    }
    "TestModel insert values SQL" should "be {stringColumn}, {optIntColumn}" in {
        TestModel.sqlInsertValueParameters(TestModel.InsertColumns) shouldBe ("{stringColumn}, {optIntColumn}")
    }
    "Quoted identifier TestModel.stringColumn" should "be \"TestModel\".\"stringColumn\"" in {
        val result = TestModel.sqlQuoteIdentifier("TestModel.stringColumn")
        result shouldBe ("\"TestModel\".\"stringColumn\"")
    }
    "Quoted identifier Test\"Model.string\"Column" should "be \"TestModel\".\"stringColumn\"" in {
        val result = TestModel.sqlQuoteIdentifier("Test\"Model.string\"Column")
        result shouldBe ("\"TestModel\".\"stringColumn\"")
    }
    "Parameter name TestModel.stringColumn" should "be TestModel.stringColumn" in {
        val result = TestModel.sqlParameterName("TestModel.stringColumn")
        result shouldBe ("TestModel.stringColumn")
    }
    "Quoted identifier Test\"Model.string\"Column" should "be TestModel.stringColumn" in {
        val result = TestModel.sqlParameterName("Test\"Model.string\"Column")
        result shouldBe ("TestModel.stringColumn")
    }

    "WHERE clause for empty QueryFilter" should "be empty" in {
        val qf = QueryFilter(filter = Map.empty[String,Seq[String]])
        val (whereClause, namedParameters@_) =  TestModel.sqlWhereClause(Some(qf))
        whereClause shouldBe ("")
    }
    "WHERE clause for foo -> Seq{bar)" should "be WHERE \"foo\" = 'bar'" in {
        val qf = QueryFilter(filter = Map("foo" -> Seq("bar")))
        val (whereClause, namedParameters@_) =  TestModel.sqlWhereClause(Some(qf))
        whereClause shouldBe ("WHERE \"foo\" = {foo}")
    }
    "WHERE clause for foo.bar -> Seq{bar)" should "be WHERE \"foo\".\"bar\" = 'bar'" in {
        val qf = QueryFilter(filter = Map("foo.bar" -> Seq("bar")))
        val (whereClause, namedParameters@_) =  TestModel.sqlWhereClause(Some(qf))
        whereClause shouldBe ("WHERE \"foo\".\"bar\" = {foo.bar}")
    }
    "WHERE clause for foo -> Seq(bar), bar -> Seq(foo)" should "be WHERE \"foo\" = 'bar'" in {
        val qf = QueryFilter(filter = Map("foo" -> Seq("bar"), "bar" -> Seq("foo")))
        val (whereClause, namedParameters@_) =  TestModel.sqlWhereClause(Some(qf))
        whereClause shouldBe ("WHERE \"foo\" = {foo} AND \"bar\" = {bar}")
    }
    "WHERE clause for foo -> Seq(bar), bar -> Seq(foo, bar)" should "be WHERE \"foo\" = {foo} AND \"bar\" IN ({bar})" in {
        val qf = QueryFilter(filter = Map("foo" -> Seq("bar"), "bar" -> Seq("foo", "bar")))
        val (whereClause, namedParameters@_) =  TestModel.sqlWhereClause(Some(qf))
        whereClause shouldBe ("WHERE \"foo\" = {foo} AND \"bar\" IN ({bar})")
    }
    "WHERE clause for foo -> Seq(bar), bar -> Seq(foo, bar), foobar -> Seq(foobar*)" should "be WHERE \"foo\" = {foo} AND \"bar\" IN ({bar}) AND \"foobar\" LIKE {foobar}" in {
        val qf = QueryFilter(filter = Map("foo" -> Seq("bar"), "bar" -> Seq("foo", "bar"), "foobar" -> Seq("foobar*")))
        val (whereClause, namedParameters@_) =  TestModel.sqlWhereClause(Some(qf))
        whereClause shouldBe ("WHERE \"foo\" = {foo} AND \"bar\" IN ({bar}) AND \"foobar\" LIKE {foobar}")
    }

    "ORDER BY clause for TestModel.stringColumn -> false" should "be ORDER BY \"TestModel\".\"stringColumn\" ASC" in {
        val qs = QuerySort(sort = Seq("TestModel.stringColumn" -> false))
        val orderByClause = TestModel.sqlOrderByClause(Some(qs))
        orderByClause shouldBe ("ORDER BY \"TestModel\".\"stringColumn\" ASC")
    }
    "ORDER BY clause for TestModel.stringColumn -> true" should "be ORDER BY \"TestModel\".\"stringColumn\" DESC" in {
        val qs = QuerySort(sort = Seq("TestModel.stringColumn" -> true))
        val orderByClause = TestModel.sqlOrderByClause(Some(qs))
        orderByClause shouldBe ("ORDER BY \"TestModel\".\"stringColumn\" DESC")
    }
    "ORDER BY clause for TestModel.stringColumn -> false, TestModel.optIntColumn ->true" should "be ORDER BY \"TestModel\".\"stringColumn\" ASC, \"TestModel\".\"optIntColumn\" DESC" in {
        val qs = QuerySort(sort = Seq("TestModel.stringColumn" -> false, "TestModel.optIntColumn" -> true))
        val orderByClause = TestModel.sqlOrderByClause(Some(qs))
        orderByClause shouldBe ("ORDER BY \"TestModel\".\"stringColumn\" ASC, \"TestModel\".\"optIntColumn\" DESC")
    }

    "SQL LIMIT and OFFSET clauses for page(10, 20)" should "be (LIMIT 20, OFFSET 180)" in {
        val page = QueryPage(number = 10, size = 20)
        val (limit, offset) = TestModel.sqlLimitAndOffsetClauses(Some(page))
        limit shouldBe ("LIMIT 20")
        offset shouldBe ("OFFSET 180")
    }
}
