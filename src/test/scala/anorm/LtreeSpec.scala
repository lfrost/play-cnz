/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

import com.cnz.play.anorm.Ltree
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/** Ltree tests
 */
class LtreeSpec extends AnyFlatSpec with Matchers {
    "Empty ltree" should "have 0 levels" in {
        val result = Ltree("")
        result.nlevel shouldBe (0)
    }

    "ltree 'com'" should "have 1 level" in {
        val result = Ltree("com")
        result.nlevel shouldBe (1)
    }

    "ltree 'com.cnz'" should "have 2 levels" in {
        val result = Ltree("com.cnz")
        result.nlevel shouldBe (2)
    }

    "ltree concatation 'com.cnz' || 'www'" should "be 'com.cnz.www'" in {
        val ltree1 = Ltree("com.cnz")
        val ltree2 = Ltree("www")
        val ltree3 = ltree1 || ltree2
        ltree3 shouldBe (Ltree("com.cnz.www"))
    }

    "ltree concatation 'com.cnz' || ''" should "be 'com.cnz'" in {
        val ltree1 = Ltree("com.cnz")
        val ltree2 = Ltree("")
        val ltree3 = ltree1 || ltree2
        ltree3 shouldBe (ltree1)
    }

    "ltree concatation '' || 'com.cnz'" should "be 'com.cnz'" in {
        val ltree1 = Ltree("")
        val ltree2 = Ltree("com.cnz")
        val ltree3 = ltree1 || ltree2
        ltree3 shouldBe (ltree2)
    }

    "ltree ancestor test '' @> 'com.cnz'" should "be true" in {
        val ltree1 = Ltree("")
        val ltree2 = Ltree("com.cnz")
        ltree1 @> ltree2 shouldBe (true)
    }

    "ltree ancestor test 'com' @> 'com.cnz'" should "be true" in {
        val ltree1 = Ltree("com")
        val ltree2 = Ltree("com.cnz")
        ltree1 @> ltree2 shouldBe (true)
    }

    "ltree ancestor test 'com.cnz' @> 'com.cnz'" should "be true" in {
        val ltree1 = Ltree("com.cnz")
        val ltree2 = Ltree("com.cnz")
        ltree1 @> ltree2 shouldBe (true)
    }

    "ltree ancestor test 'com.cnz' @> 'com'" should "be false" in {
        val ltree1 = Ltree("com.cnz")
        val ltree2 = Ltree("com")
        ltree1 @> ltree2 shouldBe (false)
    }

    "ltree ancestor test 'com.cnz' @> ''" should "be false" in {
        val ltree1 = Ltree("com.cnz")
        val ltree2 = Ltree("")
        ltree1 @> ltree2 shouldBe (false)
    }

    "ltree descendant test 'com.cnz' <@ ''" should "be true" in {
        val ltree1 = Ltree("com.cnz")
        val ltree2 = Ltree("")
        ltree1 <@ ltree2 shouldBe (true)
    }

    "ltree descendant test 'com.cnz' <@ 'com'" should "be true" in {
        val ltree1 = Ltree("com.cnz")
        val ltree2 = Ltree("com")
        ltree1 <@ ltree2 shouldBe (true)
    }

    "ltree descendant test 'com.cnz' <@ 'com.cnz'" should "be true" in {
        val ltree1 = Ltree("com.cnz")
        val ltree2 = Ltree("com.cnz")
        ltree1 <@ ltree2 shouldBe (true)
    }

    "ltree descendant test 'com' <@ 'com.cnz'" should "be false" in {
        val ltree1 = Ltree("com")
        val ltree2 = Ltree("com.cnz")
        ltree1 <@ ltree2 shouldBe (false)
    }

    "ltree descendant test '' <@ 'com.cnz'" should "be false" in {
        val ltree1 = Ltree("")
        val ltree2 = Ltree("com.cnz")
        ltree1 <@ ltree2 shouldBe (false)
    }

    "Ltree with invalid character" should "throw IllegalArgumentException" in {
        assertThrows[IllegalArgumentException] {
            Ltree(value = "$")
        }
    }

    "Ltree with label = max label length" should "succeed" in {
        val result = Ltree("a" * Ltree.MaxLabelLength)
        result.value shouldBe ("a" * Ltree.MaxLabelLength)
    }

    "Ltree with label > max label length" should "throw IllegalArgumentException" in {
        assertThrows[IllegalArgumentException] {
            Ltree("a" * (Ltree.MaxLabelLength + 1))
        }
    }

    "Ltree with levels = max length" should "succeed" in {
        val result = Ltree("a." * (Ltree.MaxLength - 1) + "a")
        result.value shouldBe ("a." * (Ltree.MaxLength - 1) + "a")
    }

    "Ltree with levels > max length" should "throw IllegalArgumentException" in {
        assertThrows[IllegalArgumentException] {
            Ltree("a." * (Ltree.MaxLength) + "a")
        }
    }
}
