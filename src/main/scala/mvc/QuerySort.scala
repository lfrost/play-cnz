/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.mvc

import play.api.mvc.QueryStringBindable

/** HTTP query parameter data type for sorting
 *
 *  This implementation adheres to the JSON:API specification and its
 *  recommendations, but does not require the use of JSON:API.
 *
 *  Below is an example of using this data type in a route definition.
 *  The recommended QueryPage parameter name is page, but this class
 *  will work with any name.
 *
 *  `GET /api/Example controllers.ExampleController.list(sort:Option[QuerySort])`
 *
 *  Assuming a parameter name of "sort", the URL would like like this
 *  (with the commas encoded).
 *
 *  `https://api.example.com/api/Example?sort=Person.nameLast,Person.nameFirst,Person.nameMiddle`
 *
 *  To reverse the sort direction of any field, prefix that field name
 *  with an ASCII minus sign (-).
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *  @see [[http://jsonapi.org/format/#fetching-sorting]]
 *
 *  @param sort Sort order as a list of tuples of the form (field-name, reverse)
 */
case class QuerySort(
    sort : Seq[(String, Boolean)]
) {
    require(!sort.exists(_._1 == ""), "Invalid field name")
}

/** QuerySort companion object
 */
object QuerySort {
    private val Reverse = '-'

    /** Takes a comma-separated sort string and converts to tuples of
     *  (column, order), where order is 1 for ascending or -1 for
     *  descending.
     *
     *  @param sort Sort order as a comma-separated list, a leading - on each to indicate DESC
     *  @return     Ordered list of sort tuples of the form (field-name, reverse)
     */
    private def sortStringToTuples(sort:String) : Seq[(String, Boolean)] = {
        sort.split(',').foldLeft(Seq.empty[(String, Boolean)])(
            (accumulator:Seq[(String, Boolean)], x:String) => {
                accumulator :+ (if (x.length > 0 && x.charAt(0) == Reverse) {
                    (x.drop(1), true)
                } else {
                    (x, false)
                })
            }
        )
    }

    /** Bind sort parameter from the HTTP query string.
     *
     *  The name key below refers to the name of the parameter specified in
     *  the route.
     *
     *  @param stringBinder Query parameter binder for type String
     *  @return             Binder for QuerySort query string parameters
     */
    implicit def queryStringBindable(implicit stringBinder:QueryStringBindable[String]) : QueryStringBindable[QuerySort] = new QueryStringBindable[QuerySort] {
        override def bind(key:String, params:Map[String, Seq[String]]) : Option[Either[String, QuerySort]] = {
            val unableMessage = s"Unable to bind $key to a QuerySort."

            stringBinder.bind(key, params) match {
                case Some(Right(sort)) => {
                    val tpls = sortStringToTuples(sort)
                    try {
                        Some(Right(QuerySort(sort = tpls)))
                    } catch {
                        case _:Throwable => Some(Left(unableMessage))
                    }
                }
                // QueryStringBindable.bindableString never returns Left(msg), so this case will always match to None.
                case _                 => None
            }
        }

        override def unbind(key:String, querySort:QuerySort) : String = {
            stringBinder.unbind(key, querySort.sort.foldLeft("")(
                (accumulator:String, x:(String, Boolean)) => {
                    accumulator +
                        (if (accumulator.length > 0) "," else "") +
                        (if (x._2 == true) Reverse else "")       +
                        x._1
                }
            ))
        }
    }
}
