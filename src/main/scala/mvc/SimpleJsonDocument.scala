/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.mvc

import com.cnz.play.http.MimeType
import play.api.libs.json.{Json, JsValue, Writes}
import play.api.mvc.Accepting

/** Simple JSON document
 *
 *  This is intended to be used as input to a function that renders HTTP
 *  responses.
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @param data data
 */
case class SimpleJsonDocument(
    data : JsValue
)

/** SimpleJsonDocument companion object
 */
object SimpleJsonDocument {
    /** HTTP request extractor for "accepts JSON" header
     */
    val AcceptsSimpleJson = Accepting(MimeType.Json.value)

    /** JSON serializer for simple JSON documents
     */
    implicit val SimpleJsonDocumentErrorWrites:play.api.libs.json.Writes[com.cnz.play.mvc.ErrorDocument] = new Writes[ErrorDocument] {
        def writes(o:ErrorDocument) =
            Json.obj(
                "errors" -> Json.arr(
                    Json.obj() ++(o.id match {
                        case None     => Json.obj()
                        case Some(id) => Json.obj("id" -> id)
                    }) ++
                    Json.obj(
                        "status" -> o.statusCode.toString,
                        "detail" -> o.exception.getMessage
                    )
                )
            )
    }

    /**
     *  @param httpError HTTP error
     *  @return          Tuple of MIME type and content
     */
    def formatSimpleJsonErrorDocument(httpError:ErrorDocument) : (MimeType, String) = {
        (MimeType.Json, Json.toJson(httpError).toString)
    }
}
