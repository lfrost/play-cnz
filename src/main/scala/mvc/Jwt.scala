/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.mvc

import java.nio.charset.StandardCharsets.UTF_8
import java.time.ZonedDateTime
import java.util.Base64
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

import com.typesafe.config.Config
import play.api.{ConfigLoader, Configuration}
import play.api.libs.json.{JsError, JsObject, Json, JsSuccess, JsValue, Reads}
import play.api.mvc.{ActionBuilder, ActionTransformer, AnyContent, BodyParsers, Request, WrappedRequest}

/** Base class for JWT exceptions
 */
abstract class JwtException extends Exception

/** JWT expired exception
 *
 *  @param exp Expiration time
 */
class JwtExpiredException(exp:Option[Long]) extends JwtException {
    override def getMessage : String = s"JWT expired at $exp."
}

/** JWT invalid signature exception
 */
class JwtInvalidSignatureException extends JwtException {
    override def getMessage : String = "JWT signature is invalid."
}

/** JWT configuration
 *
 *  The example below shows the reference configuration.
 *
 *  @example
 *  {{{
 *  cnz {
 *      jwt {
 *          # Signing algorithm
 *          # Valid values are "HS256" (for HMAC SHA-256), "RS256" (for RSA SHA-256), and "none".
 *          algorithm = "HS256"
 *
 *          # Token type
 *          # The only valid value is "JWT".  See https://tools.ietf.org/html/rfc7519#section-5.1.
 *          type      = "JWT"
 *
 *          # Signing secret
 *          secret    = "changeme"
 *      }
 *  }
 *  }}}
 *
 *  Given the default values above, it is normally necessary to set only
 *  the secret.  This can also be set using the environment variable JWT_SECRET.
 *
 *  @param algorithm Signing algorithm
 *  @param type      Token type
 *  @param secret    Signing secret
 *
 *  @throws java.lang.IllegalArgumentException if any of the constructor arguments are invalid.
 */
case class JwtConfig(
    algorithm : String,
    `type`    : String,
    secret    : String
) {
    require(JwtConfig.ValidAlgorithms.contains(algorithm),                      "Invalid algorithm")
    require(JwtConfig.ValidTypes.contains(`type`),                              "Invalid type")
    require(if (algorithm == "none") secret.length == 0 else secret.length > 0, "Invalid secret")
}

/** JwtConfig companion object
 */
object JwtConfig {
    /** Valid signing algorithms
     */
    private[mvc] val ValidAlgorithms = Seq("HS256", "RS256", "none")
    /** Valid token types
     */
    private[mvc] val ValidTypes      = Seq("JWT")

    /** Configuration loader
     *
     *  @see [[https://www.playframework.com/documentation/2.8.x/ScalaConfig#ConfigLoader]]
     */
    implicit val configLoader:ConfigLoader[JwtConfig] = new ConfigLoader[JwtConfig] {  // scalastyle:ignore field.name
        def load(rootConfig:Config, path:String) : JwtConfig = {
            val config = rootConfig.getConfig(path)
            JwtConfig(
                algorithm = config.getString("algorithm"),
                `type`    = config.getString("type"),
                secret    = config.getString("secret")
            )
        }
    }
}

/** JwtHeader companion object
 */
object JwtHeader {
    private lazy val Base64Decoder = Base64.getUrlDecoder()

    /** JSON deserializer for JwtHeader
     *
     *  @note Json.reads cannot be used here because there are multiple apply methods.
     */
    implicit val JsonReads:Reads[JwtHeader] = new Reads[JwtHeader] {
        override def reads(json:JsValue) = json match {
            case jo:JsObject => JsSuccess(JwtHeader(jo))
            case _           => JsError("error.expected.jsobject")
        }
    }

    /** JSON serializer for JwtHeader
     */
    implicit val JsonWrites:play.api.libs.json.OWrites[com.cnz.play.mvc.JwtHeader] = Json.writes[JwtHeader]

    /** Create a JWT header
     *
     *  @param jwtConfig JWT configuration
     *  @return          A JWT header
     */
    def apply()(implicit jwtConfig:JwtConfig) : JwtHeader = {
        JwtHeader(alg = jwtConfig.algorithm, typ = jwtConfig.`type`)
    }

    /** Create a JWT header from a JSON object.
     *
     *  @param jo JSON object
     *  @return   JWT header
     */
    def apply(jo:JsObject) : JwtHeader = {
        JwtHeader(
            alg = (jo \ "alg").as[String],
            typ = (jo \ "typ").as[String]
        )
    }

    /** Create JWT header from a Base64-encoded string.
     *
     *  @param s String header
     *  @return  JWT header
     *
     *  @throws java.lang.IllegalArgumentException if the header string is not valid JSON.
     */
    def fromString(s:String) : JwtHeader = {
        val json = Json.parse(new String(Base64Decoder.decode(s.getBytes(UTF_8))))
        Json.fromJson[JwtHeader](json) match {
            case JsSuccess(jp, _) => {
                jp
            }
            case JsError(_)       => {
                throw new IllegalArgumentException(s"Unable to parse JWT header $s.")
            }
        }
    }
}

/** JWT header
 *
 *  @param alg Signing algorithm
 *  @param typ Token type
 *
 *  @throws java.lang.IllegalArgumentException if any of the constructor arguments are invalid.
 */
case class JwtHeader(
    alg : String,
    typ : String
) {
    require(JwtConfig.ValidAlgorithms.contains(alg), "Invalid algorithm")
    require(JwtConfig.ValidTypes.contains(typ),      "Invalid type")

    private lazy val base64Encoder = Base64.getUrlEncoder()

    /** Convert JWT header to a Base64 encoded JSON string.
     *
     *  @return Encoded header
     */
    override def toString : String = {
        base64Encoder.encodeToString(Json.stringify(Json.toJson(this)).getBytes(UTF_8))
    }
}

/** JwtPayload companion object
 */
object JwtPayload {
    private lazy val Base64Decoder = Base64.getUrlDecoder()

    /** JSON deserializer for JwtPayload
     */
    implicit val JsonReads:Reads[JwtPayload] = Json.reads[JwtPayload]

    /** JSON serializer for JwtPayload
     */
    implicit val JsonWrites:play.api.libs.json.OWrites[com.cnz.play.mvc.JwtPayload] = Json.writes[JwtPayload]

    /** Create JWT payload from a Base64-encoded string.
     *
     *  @param s String payload
     *  @return  JWT payload
     */
    def fromString(s:String) : JwtPayload = {
        val json = Json.parse(new String(Base64Decoder.decode(s.getBytes(UTF_8))))
        Json.fromJson[JwtPayload](json) match {
            case JsSuccess(jp, _) => {
                jp
            }
            case JsError(_)       => {
                throw new IllegalArgumentException(s"Unable to parse JWT payload $s.")
            }
        }
    }
}

/** JWT payload
 *
 *  The JWT specification provides only vague definitions of the
 *  individual payload fields.  If you do not already know what values
 *  to use for the various fields (known as claims), consider the
 *  following suggestions.
 *
 *  - Use the application name as the issuer.
 *  - Use a user ID as the subject, the permissions of that user defining the permissions of the token.
 *  - For login sessions, use the session ID as the JWT ID.
 *  - For JWTs created as static tokens, use the ID of the static token.
 *  - Use audience values of "dynamic" or "static" for login sessions and static tokens, respectively.
 *
 *  This implementation supports what are referred to as public claims.
 *  It does not yet support registered claims or private claims.
 *
 *  @see [[https://www.iana.org/assignments/jwt/jwt.xhtml#claims]] for the list of public and registered claims.
 *
 *  @param iss Issuer          [[https://tools.ietf.org/html/rfc7519#section-4.1.1]]
 *  @param sub Subject         [[https://tools.ietf.org/html/rfc7519#section-4.1.2]]
 *  @param aud Audience        [[https://tools.ietf.org/html/rfc7519#section-4.1.3]]
 *  @param exp Expiration time [[https://tools.ietf.org/html/rfc7519#section-4.1.4]]
 *  @param nbf Not before      [[https://tools.ietf.org/html/rfc7519#section-4.1.5]]
 *  @param iat Issued at       [[https://tools.ietf.org/html/rfc7519#section-4.1.6]]
 *  @param jti JWT ID          [[https://tools.ietf.org/html/rfc7519#section-4.1.7]]
 */
case class JwtPayload(
    iss : Option[String] = None,
    sub : Option[String] = None,
    aud : Option[String] = None,
    exp : Option[Long]   = None,
    nbf : Option[Long]   = None,
    iat : Option[Long]   = None,
    jti : Option[String] = None
) {
    private lazy val base64Encoder = Base64.getUrlEncoder()

    /** Is expired?
     *
     *  @param strict Treat tokens with no exp claim as expired.
     *  @return       Is this JWT expired?
     */
    def isExpired(strict:Boolean = false) : Boolean = {
        exp match {
            case None      => {
                strict
            }
            case Some(exp) => {
                val now = ZonedDateTime.now().toEpochSecond()
                exp <= now
            }
        }
    }

    /** Convert JWT payload to a Base64 encoded JSON string.
     *
     *  @return Encoded payload
     */
    override def toString : String = {
        base64Encoder.encodeToString(Json.stringify(Json.toJson(this)).getBytes(UTF_8))
    }
}

/** JWT request
 *
 *  @tparam A         The body content type
 *
 *  @param jwtPayload JWT payload
 *  @param request    The HTTP request to be wrapped
 */
class JwtRequest[A](val jwtPayload:Option[JwtPayload] = None, val request:Request[A]) extends WrappedRequest[A](request)

/** JWT action
 *
 *  Verifies a JWT bearer token (an Authorization header of the form
 *  "Bearer JWT") and extracts the JWT payload into a JwtRequest.  This
 *  verification includes the signature and expiration time.
 *
 *  @see [[https://tools.ietf.org/html/rfc6750]] for information on bearer tokens.
 *
 *  @param configuration    Configuration
 *  @param parser           Body parser
 *  @param executionContext Execution context
 */
class JwtAction @Inject()(
    val configuration : Configuration,
    val parser        : BodyParsers.Default
)(implicit val executionContext:ExecutionContext)
extends ActionBuilder[JwtRequest, AnyContent] with ActionTransformer[Request, JwtRequest] {
    implicit private val jwtConfig:JwtConfig  = configuration.get[JwtConfig]("cnz.jwt")
    implicit private val defaultMarkerContext:play.api.MarkerContext = play.api.MarkerContext.NoMarker
    private          val logger               = play.api.Logger(this.getClass)

    override def transform[A](request:Request[A]) : Future[JwtRequest[A]] = Future.successful {
        request.headers.get("Authorization") match {
            case None                     => {
                new JwtRequest[A](request = request)
            }
            case Some(authorizationValue) => {
                authorizationValue.split(' ') match {
                    case Array("Bearer", signedToken) => {
                        Jwt.verify(signedToken) match {
                            case Failure(e)          => {
                                logger.info(s"Authorization header verification failed for client ${request.remoteAddress}: ${e.getMessage}")
                                new JwtRequest[A](request = request)
                            }
                            case Success(jwtPayload) => {
                                new JwtRequest(jwtPayload = Some(jwtPayload), request = request)
                            }
                        }
                    }
                    case _                            => {
                        logger.info(s"Invalid format Authorization header received from ${request.remoteAddress}: $authorizationValue")
                        new JwtRequest[A](request = request)
                    }
                }
            }
        }
    }
}

/** JSON web token
 *
 *  @see [[https://jwt.io/introduction/]] for an introduction to JWTs.
 *  @see [[https://tools.ietf.org/html/rfc7519]] for the JWT specification.
 *
 *  @param header    Base64 encoded header
 *  @param payload   Base64 encoded payload
 *  @param signature Base64 encoded signature
 */
case class Jwt(
    header    : String,
    payload   : String,
    signature : Option[String] = None
) {
    private lazy val base64Encoder = Base64.getUrlEncoder()

    override def toString : String = {
        // Note:  Unsigned JWTs include a trailing dot.  See https://tools.ietf.org/html/rfc7519#section-6.1.
        header + Jwt.TokenSep + payload + Jwt.TokenSep + signature.getOrElse("")
    }

    /** Signable content
     *
     *  The trailing dot is not part of the signed data.
     *
     *  @return The signable content for this JWT
     */
    private def signable : String = {
        header + Jwt.TokenSep + payload
    }

    /** Redact the JWT signature.
     *
     *  This is primarily for logging the JWT.
     *
     *  @return       Token with the signature redacted.
     */
    def redacted : String = {
        this.copy(signature = Some("XXXXXXXX")).toString
    }

    /** Sign this JWT
     *
     *  @param jwtConfig JWT configuration
     *  @return          A signed JWT or an exception
     */
    def sign()(implicit jwtConfig:JwtConfig) : Try[Jwt] = Try {
        jwtConfig.algorithm match {
            case "HS256" => {
                val macAlg       = "HmacSHA256"
                val jwtSecretKey = new javax.crypto.spec.SecretKeySpec(jwtConfig.secret.getBytes(UTF_8), macAlg)  // throws IllegalArgumentException
                val mac          = javax.crypto.Mac.getInstance(macAlg)                                           // throws NoSuchAlgorithmException, NoSuchProviderException
                mac.init(jwtSecretKey)                                                                            // throws InvalidKeyException
                this.copy(signature = Some(base64Encoder.encodeToString(mac.doFinal(this.signable.getBytes(UTF_8)))))
            }
            case "RS256" => {
                // Convert PKCS#8 PEM private key to a PKCS8EncodedKeySpec.
                val pkcs8Pem = jwtConfig.secret.
                    replace("-----BEGIN PRIVATE KEY-----", "").
                    replace("-----END PRIVATE KEY-----", "").
                    replaceAll("\\s+", "")
                val keySpec = new java.security.spec.PKCS8EncodedKeySpec(Base64.getDecoder().decode(pkcs8Pem))

                val kf         = java.security.KeyFactory.getInstance("RSA")           // throws NoSuchAlgorithmException
                val privateKey = kf.generatePrivate(keySpec)                           // throws InvalidKeySpecException
                val signer     = java.security.Signature.getInstance("SHA256withRSA")  // throws NoSuchAlgorithmException
                signer.initSign(privateKey)                                            // throws InvalidKeyException
                signer.update(this.signable.getBytes(UTF_8))                           // throws SignatureException
                this.copy(signature = Some(base64Encoder.encodeToString(signer.sign())))
            }
            case "none"  => this.copy(signature = None)
            case _       => throw new IllegalArgumentException(s"Unknown signing algorithm ${jwtConfig.algorithm}.")
        }
    }
}

/** Jwt companion object
 */
object Jwt {
    private val TokenSep = '.'

    /** Create a `Jwt` from a header and a payload.
     *
     *  @param jwtHeader  Header
     *  @param jwtPayload Payload
     *  @return           JWT
     */
    def apply(jwtHeader:JwtHeader, jwtPayload:JwtPayload) : Jwt = {
        Jwt(header = jwtHeader.toString, payload = jwtPayload.toString)
    }

    /** Create a `Jwt` from a Base64-encoded JWT string.
     *
     *  @param token Token
     *  @return      JWT
     *
     *  @throws java.lang.IllegalArgumentException if the token does not have three fields.
     */
    def apply(token:String) : Jwt = {
        token.split(TokenSep) match {
            case Array(header, payload, signature) => {
                Jwt(header = header, payload = payload, signature = Some(signature))
            }
            case _                                 => {
                throw new IllegalArgumentException(s"Token must have 3 fields separated by $TokenSep.")
            }
        }
    }

    /** Verify signature of a JWT.
     *
     *  This includes checking the exp claim, if present in the JWT.
     *
     *  @param signedToken Signed JSON web token
     *  @param jwtConfig   JWT configuration
     *  @return            The JWT payload or an exception
     */
    def verify(signedToken:String)(implicit jwtConfig:JwtConfig) : Try[JwtPayload] = Try {
        val jwt = Jwt(signedToken)
        jwt.sign() match {
            case Success(jwtVerify) => {
                if (jwt.signature == jwtVerify.signature) {
                    val jwtPayload = JwtPayload.fromString(jwt.payload)
                    if (jwtPayload.isExpired()) {
                        throw new JwtExpiredException(jwtPayload.exp)
                    } else {
                        jwtPayload
                    }
                } else {
                    throw new JwtInvalidSignatureException
                }
            }
            case Failure(e)         => throw e
        }
    }
}
