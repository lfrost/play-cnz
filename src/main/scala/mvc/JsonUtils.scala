/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.mvc

import java.net.{MalformedURLException, URI, URISyntaxException, URL}

import play.api.libs.json.{JsError, Json, JsSuccess, JsValue, Reads, Writes}

/** JSON deserializers and serializers
 */
object JsonUtils {
    /** JSON deserializer for URI
     */
    implicit val UriReads:Reads[URI] = new Reads[URI] {
        override def reads(json:JsValue) = try {
            JsSuccess(new URI(json.as[String]))
        } catch {
            case _:URISyntaxException => JsError("Invalid URI")
        }
    }

    /** JSON serializer for URI
     */
    implicit val UriWrites:Writes[URI] = new Writes[URI] {
        override def writes(o:URI) = Json.toJson(o.toString())
    }

    /** JSON deserializer for URL
     */
    implicit val UrlReads:Reads[URL] = new Reads[URL] {
        override def reads(json:JsValue) = try {
            JsSuccess(new URL(json.as[String]))
        } catch {
            case _:MalformedURLException => JsError("Invalid URL")
        }
    }

    /** JSON serializer for URL
     */
    implicit val UrlWrites:Writes[URL] = new Writes[URL] {
        override def writes(o:URL) = Json.toJson(o.toString())
    }
}
