/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.mvc

import play.api.mvc.QueryStringBindable

/** HTTP query parameter data type for filtering
 *
 *  This implementation adheres to the JSON:API specification and its
 *  recommendations, but does not require the use of JSON:API.
 *
 *  Below is an example of using this data type in a route definition.
 *  The recommended QueryPage parameter name is filter, but this class
 *  will work with any name.
 *
 *  `GET /api/Example controllers.ExampleController.list(filter:Option[QueryFilter])`
 *
 *  Assuming a parameter name of "filter", the URL would look like this
 *  (with [ and ] encoded).
 *
 *  `https://api.example.com/api/Example?filter[Person.nameLast]=Howard&filter[Person.nameFirst]=Robert&filter[Person.nameFirst]=Bob`
 *
 *  Assuming an SQL database and filter names of the form Table.column,
 *  this would normally be interpreted by the application as
 *
 *  `"Person"."nameLast" = 'Howard' AND "Person"."nameFirst" IN ('Robert', 'Bob')`
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *  @see [[http://jsonapi.org/format/#fetching-filtering]]
 *  @see [[http://jsonapi.org/recommendations/#filtering]]
 *
 *  @param filter Filters as a map of the form field-name -> values
 */
case class QueryFilter(
    filter : Map[String, Seq[String]]
) {
    require(!filter.exists(_._1 == ""), "Invalid filter name")
}

/** QueryFilter companion object
 */
object QueryFilter {
    /** Bind filter parameters from the HTTP query string.
     *
     *  The name key below refers to the name of the parameter specified in the
     *  route.
     *
     *  @param seqStringBinder Query parameter binder for type Seq[String]
     *  @return                Binder for QueryFilter query string parameters
     */
    implicit def queryStringBindable(implicit seqStringBinder:QueryStringBindable[Seq[String]]) : QueryStringBindable[QueryFilter] = new QueryStringBindable[QueryFilter] {
        override def bind(key:String, params:Map[String, Seq[String]]) : Option[Either[String, QueryFilter]] = {
            val pattern = (key + "\\[(.+)\\]").r

            // Build filter with entries of the form keyIndex -> Seq(values).
            val filter = params.foldLeft(Map.empty[String, Seq[String]])(
                (accumulator:Map[String, Seq[String]], x:(String, Seq[String])) => {
                    x._1 match {
                        case pattern(keyIndex) => {
                            seqStringBinder.bind(x._1, params) match {
                                case (Some(Right(ss))) => accumulator + (keyIndex -> ss)
                                case _                 => accumulator
                            }
                        }
                        case _                 => accumulator
                    }
                }
            )

            if (filter.isEmpty) {
                None
            } else {
                Some(Right(QueryFilter(filter = filter)))
            }
        }

        override def unbind(key:String, queryFilter:QueryFilter) : String = {
            queryFilter.filter.foldLeft("")(
                (accumulator:String, x:(String, Seq[String])) => {
                    accumulator +
                        (if (accumulator.length > 0) "&" else "") +
                        seqStringBinder.unbind(s"$key[${x._1}]", x._2)
                }
            )
        }
    }
}
