/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.mvc

import play.api.mvc.QueryStringBindable

/** HTTP query parameter data type for pagination
 *
 *  This implementation adheres to the JSON:API specification and its
 *  recommendations, but does not require the use of JSON:API.
 *
 *  Below is an example of using this data type in a route definition.
 *  The recommended QueryPage parameter name is page, but this class
 *  will work with any name.
 *
 *  `GET /api/Example controllers.ExampleController.list(page:Option[QueryPage]`
 *
 *  Assuming a parameter name of "page", the URL would look like this
 *  (with [ and ] encoded).
 *
 *  `https://api.example.com/api/Example?page[number]=1&page[size]=10`
 *
 *  The page number must be 1-based.
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *  @see [[http://jsonapi.org/format/#fetching-pagination]]
 *
 *  @param number Page number (1-based)
 *  @param size   Page size (number of records)
 */
case class QueryPage(
    number : Long,
    size   : Int
) {
    require(number > 0L, "Invalid page number")
    require(size > 0,    "Invalid page size")

    /** Test if this page is first.
     *
     *  @return  Is this the first page?
     */
    def isFirst : Boolean = number == 1

    /** Is this the first page?
     *
     *  @return First page
     */
    def first : QueryPage = {
        this.copy(number = 1)
    }

    /** Create last page relative to this page.
     *
     *  @param pageCount Number of pages
     *  @return          Last page
     */
    def last(pageCount:Long) : Option[QueryPage] = {
        if (pageCount > 0L) {
            Some(this.copy(number = pageCount))
        } else {
            None
        }
    }

    /** Create next page relative to this page.
     *
     *  @return Next page
     */
    def next : QueryPage = {
        this.copy(number = this.number + 1)
    }

    /** Create next page relative to this page.
     *
     *  @param pageCount Number of pages
     *  @return          Next page
     */
    def next(pageCount:Long) : Option[QueryPage] = {
        if (this.number < pageCount) {
            Some(this.copy(number = this.number + 1))
        } else {
            None
        }
    }

    /** Create previous page relative to this page.
     *
     *  @return Previous page
     */
    def prev : Option[QueryPage] = {
        if (this.number > 1) {
            Some(this.copy(number = this.number - 1))
        } else {
            None
        }
    }
}

/** QueryPage companion object
 */
object QueryPage {
    private val NumberIndex = "[number]"
    private val SizeIndex   = "[size]"

    /** Bind page parameters from the HTTP query string.
     *
     *  The name key below refers to the name of the parameter specified in the
     *  route.
     *
     *  @param intBinder  Query parameter binder for type Int
     *  @param longBinder Query parameter binder for type Long
     *  @return           Binder for QueryPage query string parameters
     */
    implicit def queryStringBindable(implicit intBinder:QueryStringBindable[Int], longBinder:QueryStringBindable[Long]) : QueryStringBindable[QueryPage] = new QueryStringBindable[QueryPage] {
        override def bind(key:String, params:Map[String, Seq[String]]) : Option[Either[String, QueryPage]] = {
            val unableMessage = s"Unable to bind $key to a QueryPage."

            for {
                number <- longBinder.bind(key + NumberIndex, params)
                size   <- intBinder.bind(key + SizeIndex, params)
            } yield {
                (number, size) match {
                    case (Right(number), Right(size)) => {
                        try {
                            Right(QueryPage(number = number, size = size))
                        } catch {
                            case _:Throwable => Left(unableMessage)
                        }
                    }
                    case _                            => Left(unableMessage)
                }
            }
        }

        override def unbind(key:String, queryPage:QueryPage) : String = {
            longBinder.unbind(key + NumberIndex, queryPage.number) + "&" + intBinder.unbind(key + SizeIndex, queryPage.size)
        }
    }
}
