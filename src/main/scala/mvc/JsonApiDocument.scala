/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.mvc

import com.cnz.play.http.MimeType
import play.api.libs.json.{JsObject, Json, JsValue, Writes}
import play.api.mvc.Accepting

/** JSON:API document
 *
 *  This is intended to be used as input to a function that renders HTTP
 *  responses.
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *  @see [[https://jsonapi.org/format/]]
 *
 *  @param self     self link
 *  @param first    first page link
 *  @param prev     previous page link
 *  @param next     next page link
 *  @param last     last page link
 *  @param data     data
 *  @param included included
 */
case class JsonApiDocument(
    self     : Option[String]  = None,
    first    : Option[String]  = None,
    prev     : Option[String]  = None,
    next     : Option[String]  = None,
    last     : Option[String]  = None,
    data     : JsValue,
    included : Option[JsValue] = None
)

/** JsonApiDocument companion object
 */
object JsonApiDocument {
    /** HTTP request extractor for "accepts JSON:API" header
     */
    val AcceptsJsonApi = Accepting(MimeType.JsonApi.value)

    /** JSON serializer for JSON:API documents
     */
    implicit val JsonApiDocumentWrites:play.api.libs.json.Writes[com.cnz.play.mvc.JsonApiDocument] = new Writes[JsonApiDocument] {
        def writes(o:JsonApiDocument) = {
            val links = JsObject.empty ++
                (o.self match {
                    case Some(self) => Json.obj("self" -> self)
                    case None       => JsObject.empty
                }) ++
                (o.first match {
                    case Some(first) => Json.obj("first" -> first)
                    case None        => JsObject.empty
                }) ++
                (o.prev match {
                    case Some(prev) => Json.obj("prev" -> prev)
                    case None       => JsObject.empty
                }) ++
                (o.next match {
                    case Some(next) => Json.obj("next" -> next)
                    case None       => JsObject.empty
                }) ++
                (o.last match {
                    case Some(last) => Json.obj("last" -> last)
                    case None       => JsObject.empty
                })

            Json.obj(
                "jsonapi" -> Json.obj(
                    "version" -> "1.0"
                )
            ) ++
            (links match {
                case x:JsObject if x == JsObject.empty => JsObject.empty
                case _                                 => Json.obj("links" -> links)
            }) ++
            Json.obj(
                "data" -> o.data
            ) ++
            (o.included match {
                case Some(inc) => Json.obj("included" -> inc)
                case _         => JsObject.empty
            })
        }
    }

    /**
     */
    implicit val JsonApiErrorDocumentWrites:play.api.libs.json.Writes[com.cnz.play.mvc.ErrorDocument] = new Writes[ErrorDocument] {
        def writes(o:ErrorDocument) =
            Json.obj(
                "jsonapi" -> Json.obj(
                    "version" -> "1.0"
                )
            ) ++
            Json.obj(
                "errors" -> Json.arr(
                    Json.obj() ++(o.id match {
                        case None     => Json.obj()
                        case Some(id) => Json.obj("id" -> id)
                    }) ++
                    Json.obj(
                        "status" -> o.statusCode.toString,
                        "detail" -> o.exception.getMessage
                    )
                )
            )
    }

    /**
     *  @param httpError HTTP error
     *  @return          Tuple of MIME type and content
     */
    def formatJsonApiErrorDocument(httpError:ErrorDocument) : (MimeType, String) = {
        (MimeType.JsonApi, Json.toJson(httpError).toString)
    }
}
