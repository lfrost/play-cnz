/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.clients

import java.net.URI
import javax.inject.{Inject}

import scala.concurrent.ExecutionContext

import com.cnz.play.configs.AwsConfig
import play.api.{Configuration, Logger}
import software.amazon.awssdk.services.s3.model.Bucket

/** AWS S3 client.
 *
 *  @param configuration    configuration
 *  @param executionContext execution context
 */
class AwsS3Client @Inject()(
    val configuration : Configuration
)(implicit executionContext:ExecutionContext) extends ObjectStorageClient {
    private val awsConfig = configuration.get[AwsConfig]("aws")

    override protected val logger    = Logger(this.getClass)
    override protected val endpoint  = new URI(s"https://s3.${awsConfig.region.toString}.amazonaws.com")
    override protected val accessKey = awsConfig.credentials.accessKey
    override protected val secretKey = awsConfig.credentials.secretKey

    override val region = awsConfig.region

    override def mkUrl(bucket: Bucket, key:String) : String = {
        s"https://s3.amazonaws.com/$bucket/$key"
    }
}
