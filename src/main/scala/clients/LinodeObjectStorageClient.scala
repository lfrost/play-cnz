/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.clients

import java.net.URI
import javax.inject.{Inject}

import scala.concurrent.ExecutionContext

import com.cnz.play.configs.LinodeConfig
import play.api.{Configuration, Logger}
import software.amazon.awssdk.services.s3.model.Bucket

/** Linode Object Storage client.
 *
 *  @param configuration    configuration
 *  @param executionContext execution context
 */
class LinodeObjectStorageClient @Inject()(
    val configuration : Configuration
)(implicit executionContext:ExecutionContext) extends ObjectStorageClient {
    private val linodeConfig = configuration.get[LinodeConfig]("linode")

    override protected val logger    = Logger(this.getClass)
    override protected val endpoint  = new URI(s"https://${linodeConfig.region.toString}-1.linodeobjects.com")
    override protected val accessKey = linodeConfig.credentials.accessKey
    override protected val secretKey = linodeConfig.credentials.secretKey

    override val region = linodeConfig.region

    override def mkUrl(bucket: Bucket, key:String) : String = {
        s"https://$bucket.${region.toString}-1.linodeobjects.com/$key"
    }
}
