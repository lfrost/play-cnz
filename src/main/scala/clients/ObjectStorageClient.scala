/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.clients

import java.lang.AutoCloseable
import java.net.URI
import java.nio.file.Path

import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters._             // scalastyle:ignore underscore.import
import scala.jdk.FutureConverters._                 // scalastyle:ignore underscore.import
import scala.util.{Failure, Success}

import play.api.Logger
import software.amazon.awssdk.auth.credentials.{AwsBasicCredentials, StaticCredentialsProvider}
import software.amazon.awssdk.core.ResponseBytes
import software.amazon.awssdk.core.async.AsyncResponseTransformer
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3AsyncClient
import software.amazon.awssdk.services.s3.model.{Bucket, GetObjectRequest, GetObjectResponse, HeadObjectRequest, ListBucketsRequest, ListBucketsResponse, NoSuchKeyException, ObjectCannedACL, PutObjectRequest}

/** Object Storage client.
 *
 *  @see [[https://aws.amazon.com/sdk-for-java/]]
 *  @see [[https://sdk.amazonaws.com/java/api/latest/]]
 *
 *  @param executionContext execution context
 */
abstract class ObjectStorageClient(implicit executionContext:ExecutionContext) extends AutoCloseable {
    /** S3 SDK endpoint
     */
    protected val endpoint : URI

    /** logger
     */
    protected val logger : Logger

    /** S3 access key
     */
    protected val accessKey : String

    /** S3 secret key
     */
    protected val secretKey : String

    /** S3 region
     */
    val region : Region

    /** Make a URL for an object.
     *
     *  @param bucket bucket
     *  @param key    key
     *  @return       the URL
     */
    def mkUrl(bucket:Bucket, key:String) : String

    private lazy val awsBasicCredentials = AwsBasicCredentials.create(
        accessKey,
        secretKey
    )

    /** Close this client.
     */
    def close() : Unit = {
        client.close()
    }

    /** S3 client
     */
    protected lazy val client:S3AsyncClient = {
        val client = S3AsyncClient.builder().
            endpointOverride(endpoint).
            credentialsProvider(StaticCredentialsProvider.create(awsBasicCredentials)).
            region(region).
            build()

        client
    }

    /** Download an object from S3.
     *
     *  @param bucket bucket
     *  @param key    key
     *  @return       file contents, or an exception
     */
    def download(bucket: Bucket, key:String) : Future[Array[Byte]] = {
        logger.info(s"download(key = $key)")

        val getObjectRequest = GetObjectRequest.
            builder().
            bucket(bucket.toString).
            key(key).
            build()

        val asyncResponseTransformer:AsyncResponseTransformer[GetObjectResponse, ResponseBytes[GetObjectResponse]] = AsyncResponseTransformer.toBytes()

        client.getObject(getObjectRequest, asyncResponseTransformer).asScala map { _.asByteArray }
    }

    /** Check if an object exists in S3.
     *
     *  @param bucket bucket
     *  @param key    the S3 key to check
     *  @return       true if exists, false if does not
     */
    def exists(bucket:Bucket, key:String) : Future[Boolean] = {
        client.headObject(
            HeadObjectRequest.
                builder().
                bucket(bucket.toString).
                key(key).
                build()
        ).asScala transform {
            case Success(_)                    => Success(true)
            case Failure(_:NoSuchKeyException) => Success(false)
            case Failure(e)                    => Failure(e)
        }
    }

    /** List all buckets visible in current region.
     *
     *  @return list of buckets in the current region
     */
    def listBuckets() : Future[Seq[Bucket]] = {
        val listBucketsRequest = ListBucketsRequest.builder.build()
        val listBucketsResponse: Future[ListBucketsResponse] = client.listBuckets(listBucketsRequest).asScala
        listBucketsResponse.map(_.buckets.asScala.toSeq)
    }

    /** Upload an object to S3.
     *
     *  @param bucket bucket
     *  @param key    key
     *  @param path   path
     *  @return       URL, or an exception
     */
    def upload(bucket:Bucket, key:String, path:Path) : Future[String] = {
        logger.info(s"upload(key = $key, path = $path)")

        val putObjectRequest = PutObjectRequest.
            builder().
            bucket(bucket.toString).
            key(key).
            acl(ObjectCannedACL.PUBLIC_READ).
            build()

        client.putObject(putObjectRequest, path).asScala map { _ =>
            mkUrl(bucket, key)
        }
    }
}
