/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.http

/** Add MIME types to the Play Framework list.
 */
trait MimeTypes extends play.api.http.MimeTypes {
    // scalastyle:off field.name
    // Keeping field names consistent with play.api.http.MimeTypes.
    /** JSON:API
     */
    val JSON_API = "application/vnd.api+json"
    // scalastyle:on field.name
}

/** Mimetypes object
 */
object MimeTypes extends MimeTypes

/** MIME type
 *
 *  @param value String value of this MIME type
 */
case class MimeType(value:String) {
    /** Override toString to return unwrapped value.
     *
     *  @return The unwrapped value string
     */
    override def toString : String = value
}

/** MimeType companion object
 */
object MimeType {
    /** Empty / undefined
     */
    val Empty         = MimeType("")

    /** Plain text
     */
    val Text          = MimeType(MimeTypes.TEXT)

    /** HTML
     */
    val Html          = MimeType(MimeTypes.HTML)

    /** Plain JSON
     */
    val Json          = MimeType(MimeTypes.JSON)

    /** XML
     */
    val Xml           = MimeType(MimeTypes.XML)

    /** XHTML
     */
    val Xhtml         = MimeType(MimeTypes.XHTML)

    /** CSS
     */
    val Css           = MimeType(MimeTypes.CSS)

    /** JavaScript
     */
    val JavaScript    = MimeType(MimeTypes.JAVASCRIPT)

    /** Form-urlencoded
     */
    val Form          = MimeType(MimeTypes.FORM)

    /** Server-sent events
     */
    val EventStream   = MimeType(MimeTypes.EVENT_STREAM)

    /** Binary
     */
    val Binary        = MimeType(MimeTypes.BINARY)

    /** Application cache
     */
    val CacheManifest = MimeType(MimeTypes.CACHE_MANIFEST)

    /** JSON:API
     */
    val JsonApi       = MimeType(MimeTypes.JSON_API)
}
