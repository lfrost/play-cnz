/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.configs

import com.typesafe.config.Config
import play.api.ConfigLoader
import software.amazon.awssdk.regions.Region

/** Linode credentials configuration
 *
 *  @param accessKey access key
 *  @param secretKey secret key
 */
case class LinodeCredentialsConfig(
    accessKey : String,
    secretKey : String
)

/** Linode Credentials configuration companion object
 */
object LinodeCredentialsConfig {
    /** Convert config to LinodeCredentialsConfig object
     *
     *  @param config configuration
     */
    private[configs] def fromConfig(config:Config) = {
        LinodeCredentialsConfig(
            accessKey = config.getString("accessKey"),
            secretKey = config.getString("secretKey")
        )
    }

    /**
     */
    implicit val configLoader:ConfigLoader[LinodeCredentialsConfig] = new ConfigLoader[LinodeCredentialsConfig] {  // scalastyle:ignore field.name
        def load(rootConfig:Config, path:String) : LinodeCredentialsConfig = {
            val config = rootConfig.getConfig(path)
            LinodeCredentialsConfig.fromConfig(config)
        }
    }
}

/** Linode configuration
 *
 *  @param region      region
 *  @param credentials credentials
 */
case class LinodeConfig(
    region      : Region,
    credentials : LinodeCredentialsConfig
)

/** Linode configuration companion object
 */
object LinodeConfig {
    /**
     */
    implicit val configLoader:ConfigLoader[LinodeConfig] = new ConfigLoader[LinodeConfig] {  // scalastyle:ignore field.name
        def load(rootConfig:Config, path:String) : LinodeConfig = {
            val config = rootConfig.getConfig(path)
            LinodeConfig(
                region      = Region.of(config.getString("region")),
                credentials = LinodeCredentialsConfig.fromConfig(config.getConfig("credentials"))
            )
        }
    }
}
