/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.configs

import com.typesafe.config.Config
import play.api.ConfigLoader
import software.amazon.awssdk.regions.Region

/** Aws credentials configuration
 *
 *  @param accessKey access key
 *  @param secretKey secret key
 */
case class AwsCredentialsConfig(
    accessKey : String,
    secretKey : String
)

/** Aws Credentials configuration companion object
 */
object AwsCredentialsConfig {
    /** Convert config to AwsCredentialsConfig object
     *
     *  @param config configuration
     */
    private[configs] def fromConfig(config:Config) = {
        AwsCredentialsConfig(
            accessKey = config.getString("accessKey"),
            secretKey = config.getString("secretKey")
        )
    }

    /**
     */
    implicit val configLoader:ConfigLoader[AwsCredentialsConfig] = new ConfigLoader[AwsCredentialsConfig] {  // scalastyle:ignore field.name
        def load(rootConfig:Config, path:String) : AwsCredentialsConfig = {
            val config = rootConfig.getConfig(path)
            AwsCredentialsConfig.fromConfig(config)
        }
    }
}

/** Aws configuration
 *
 *  @param region      region
 *  @param credentials credentials
 */
case class AwsConfig(
    region      : Region,
    credentials : AwsCredentialsConfig
)

/** Aws configuration companion object
 */
object AwsConfig {
    /**
     */
    implicit val configLoader:ConfigLoader[AwsConfig] = new ConfigLoader[AwsConfig] {  // scalastyle:ignore field.name
        def load(rootConfig:Config, path:String) : AwsConfig = {
            val config = rootConfig.getConfig(path)
            AwsConfig(
                region      = Region.of(config.getString("region")),
                credentials = AwsCredentialsConfig.fromConfig(config.getConfig("credentials"))
            )
        }
    }
}
