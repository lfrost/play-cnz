/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play

import play.api.inject.Binding

/** CNZ API
 *
 *  Practical extensions for Play Framework.
 */
class CnzApi

/** CNZ Module
 */
class CnzModule extends play.api.inject.Module {
    /** Get the bindings provided by this module.
     *
     *  @param environment   Play environment
     *  @param configuration Play configuration
     *  @return              List of DI bindings
     */
    override def bindings(environment:play.api.Environment, configuration:play.api.Configuration) : Seq[Binding[_]] = {
        Seq(bind[CnzApi].toInstance(new CnzApi))
    }
}
