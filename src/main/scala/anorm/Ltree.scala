/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.anorm

import java.sql.PreparedStatement

import scala.util.{Failure, Success, Try}

import anorm.{Column, ParameterMetaData, ToStatement}
import org.postgresql.util.PGobject

/** Postgres ltree
 *
 *  ltree is a Postgres data type for "representing labels of data
 *  stored in a hierarchical tree-like structure."
 *
 *  Where applicable, method names are chosen to match the
 *  corresponding ltree operator or function.
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *  @see [[https://www.postgresql.org/docs/current/static/ltree.html]]
 *
 *  @param value String representation of an ltree
 *
 *  @throws java.lang.IllegalArgumentException if the string value is invalid.
 */
case class Ltree(value:String) {
    // Validate on construction.
    require(Ltree.valid(value), s"Invalid ltree specified: $toString")

    /** Override toString to return unwrapped value.
     *
     *  @return The unwrapped value string
     */
    override def toString : String = value

    /** Test if an Ltree is an ancestor of another
     *
     *  @see [[https://www.postgresql.org/docs/current/static/ltree.html#LTREE-OP-TABLE]]
     *
     *  @param that ltree to compare to this
     *  @return     Is this an ancestor or equal to that?
     */
    def @>(that:Ltree) : Boolean = {  // scalastyle:ignore method.name
        this.value.length == 0            ||  // Empty ltree is parent of all.
        that.value.startsWith(this.value) && (
            that.value.length == this.value.length     ||  // this and that are equal.
            that.value(this.value.length) == Ltree.Sep     // this ends on a label boundary in that.
        )
    }

    /** Test if an Ltree is a descendant of another
     *
     *  @see [[https://www.postgresql.org/docs/current/static/ltree.html#LTREE-OP-TABLE]]
     *
     *  @param that ltree to compare to this
     *  @return     Is this a descendant or equal to that?
     */
    def <@(that:Ltree) : Boolean = {  // scalastyle:ignore method.name
        that @> this
    }

    /** Concatenate two ltrees
     *
     *  @see [[https://www.postgresql.org/docs/current/static/ltree.html#LTREE-OP-TABLE]]
     *
     *  @param s String representation of an ltree to append to this
     *  @return  The combined ltree
     */
    def ||(s:String) : Ltree = {  // scalastyle:ignore method.name
        (value, s) match {
            case ("", t)  => Ltree(t)
            case (_, "")  => this
            case (t1, t2) => Ltree(t1 + Ltree.Sep + t2)
        }
    }

    /** Concatenate two ltrees
     *
     *  @see [[https://www.postgresql.org/docs/current/static/ltree.html#LTREE-OP-TABLE]]
     *
     *  @param that ltree to append to this
     *  @return     The combined ltree
     */
    def ||(that:Ltree) : Ltree = {  // scalastyle:ignore method.name
        this || that.toString
    }

    /** Number of levels / labels in this Ltree
     *
     *  @see [[https://www.postgresql.org/docs/current/static/ltree.html#LTREE-FUNC-TABLE]]
     *
     *  @return Number of levels in this ltree
     */
    def nlevel : Int = {
        if (value.length == 0) {
            0
        } else {
            value.split(Ltree.Sep).length
        }
    }
}

/** Ltree companion object
 */
object Ltree {
    /** Maximum label length
     */
    val MaxLabelLength    = 256

    /** Maximum ltree length (levels)
     */
    val MaxLength         = 66559

    /** Label separator
     */
    val Sep               = '.'

    /** Pattern for valid labels
     */
    protected val LabelRe = """^[A-Za-z0-9_]+$""".r

    /** Validate an ltree.
     *
     *  @param ltree String representation of an ltree
     *  @return      Is ltree a valid ltree?
     */
    protected def valid(ltree:String) : Boolean = {
        val a = ltree.split(Sep)
        if (ltree.length == 0) {
            // empty path
            true
        } else if (a.length > MaxLength) {
            // Overall length exceeds max.
            false
        } else if (a.exists(_.length > MaxLabelLength)) {
            // Length of one or more labels exceeds max.
            false
        } else {
            // Check if one or more labels invalid.
            !(a.exists(_ match {
                case LabelRe() => false
                case _         => true
            }))
        }
    }

    /** Metadata about the custom parameter type Ltree
     *
     *  @see [[https://playframework.github.io/anorm/#custom-parameter-conversions]]
     *
     *  @return An Anorm ParameterMetaData for an Ltree
     */
    implicit def ltreeParamMeta : ParameterMetaData[Ltree] = new ParameterMetaData[Ltree] {
        override val sqlType  = "ltree"
        override def jdbcType = java.sql.Types.OTHER
    }

    /** Custom conversion from JDBC column to Ltree
     *
     *  @see [[https://playframework.github.io/anorm/#column-parsers]]
     *
     *  @return An Anorm Ltree column
     */
    implicit def columnToLtree : Column[Ltree] = {
        Column.nonNull { (value, meta) =>
            // Extract the column name and JDBC class name from the column meta data.
            val anorm.MetaDataItem(column, nullable@_, clazz) = meta

            value match {
                case pgo:PGobject if pgo.getType() == ltreeParamMeta.sqlType => Try { Ltree(pgo.getValue()) } match {
                    case Success(v) => Right(v)
                    case Failure(e) => Left(anorm.TypeDoesNotMatch(s"Cannot convert $column::$clazz value $value to Ltree: ${e.getMessage}"))
                }
                case _                                                       => Left(anorm.TypeDoesNotMatch(s"Cannot convert $column::$clazz value $value to Ltree."))
            }
        }
    }

    /** Custom conversion to statement for type Ltree
     *
     *  This makes casts of values unnecessary (e.g., in INSERT statements).
     *
     *  @see [[https://playframework.github.io/anorm/#custom-parameter-conversions]]
     *
     *  @return An Anorm ToStatement for an Ltree
     */
    implicit def ltreeToStatement : ToStatement[Ltree] = new ToStatement[Ltree] {
        override def set(statement:PreparedStatement, index:Int, value:Ltree) : Unit = {
            if (value != null) {  // scalastyle:ignore null
                val pgo = new PGobject()
                pgo.setType(ltreeParamMeta.sqlType)
                pgo.setValue(value.toString)
                statement.setObject(index, pgo)
            } else {
                statement.setNull(index, ltreeParamMeta.jdbcType)
            }
        }
    }
}
