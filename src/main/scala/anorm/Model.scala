/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.anorm

import java.sql.{Connection => DbConnection}
import java.util.UUID

import scala.util.Try

import anorm.{NamedParameter, ResultSetParser, RowParser, SQL, ToSql}
import com.cnz.play.mvc.{QueryFilter, QueryPage, QuerySort}

/** Database model
 *
 *  This class facilitates the use of Anorm.  It is to be extended for
 *  each type of database server (e.g., the ModelPostgres class), and
 *  then that is extended by each model class in an application using
 *  that database server.  Methods are included for basic CRUD
 *  operations on a single table, as well as for constructing SQL
 *  statement clauses, utilizing the com.cnz.play.mvc._ query
 *  string classes.
 *
 *  @see [[https://playframework.github.io/anorm/]]
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @tparam M Model class
 *  @tparam P Primary key class
 */
abstract class Model[M, P] {
    /** Reference for the companion of M.
     *
     *  @return The companion object of this class
     */
    def companion : ModelCompanion[M, P]
}

/** Model companion object
 *
 *  This is extended by Model companion objects.
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @tparam M Model class
 *  @tparam P Primary key class
 */
abstract class ModelCompanion[M, P] {
    // scalastyle:off field.name
    implicit private val DefaultMarkerContext:play.api.MarkerContext = play.api.MarkerContext.NoMarker
    private          val Logger               = play.api.Logger(this.getClass)

    /** Automatically cast for UUID columns as necessary.
     *
     *  @see http://playframework.github.io/anorm/unidoc/anorm/ToSql$.html
     */
    implicit val UuidToSql : ToSql[UUID]

    /** List separator string for lists of column names, values, etc.
     */
    protected val ListSep = ", "

    /** Characters to strip from identifiers (regex)
     */
    final protected val CharsToStripFromIdentifier = "[{}\"'.\\\\ ]"

    /** Characters to strip from placeholder names (regex)
     */
    final protected val CharsToStripFromPlaceholder = "[{}\"'\\\\ ]"

    /** Nil UUID
     */
    final val UuidNil = new UUID(0L, 0L)

    /** Quote character for identifiers
     */
    val QuoteId = '"'

    /** Quote character for string literal values
     */
    val QuoteVal = '\''

    /** Name of the primary database table for this model
     */
    val Table : String

    /** Name of the primary key column for this model
     */
    val PrimaryKey : String = "id"

    /** Character to use for wildcards in QueryFilter
     *
     *  This is not the wildcard for SQL.
     */
    val wildcard : String = "*"

    /** Result set parser to use for inserts.
     *
     *  This is typically set to parse a single UUID or Long, depending on the type of the primary key.
     *
     *  @example
     *  {{{
     *  override val PrimaryKeyResultSetParser : ResultSetParser[UUID] = scalar[UUID].single
     *  }}}
     *
     *  @example
     *  {{{
     *  override val PrimaryKeyResultSetParser : ResultSetParser[Long] = scalar[Long].single
     *  }}}
     */
    val PrimaryKeyResultSetParser : ResultSetParser[P]

    /** List of column names to use for INSERT
     *
     *  This list should not include columns which are database-assigned,
     *  which typically includes id and whenCreated.
     *
     *  @example
     *  {{{
     *  override val InsertColumns = Seq(
     *      "nameLast", "nameFirst"
     *  )
     *  }}}
     */
    val InsertColumns : Seq[String]

    /** List of column values to use for INSERT
     *
     *  @example
     *  {{{
     *  override def insertValues(person:Person) : Seq[NamedParameter] = {
     *      Seq(
     *          'nameLast  -> person.nameLast,
     *          'nameFirst -> person.nameFirst
     *      )
     *  }
     *  }}}
     *
     *  @param m The Model to insert
     *  @return  Seq of named parameters
     */
    def insertValues(m:M) : Seq[NamedParameter]

    /** Anorm row parser
     *
     *  Include the table names to allow the parser to be combined with
     *  other parsers for joins.
     *
     *  @example
     *  {{{
     *  override val RowParser = {
     *  get[Option[UUID]]("Person.id")                   ~
     *  get[Option[ZonedDateTime]]("Person.whenCreated") ~
     *  get[Option[String]]("Person.nameLast")           ~
     *  get[Option[String]]("Person.nameFirst")          map {
     *      case id ~ whenCreated ~ nameLast ~ nameFirst =>
     *      Person(id = id, whenCreated = whenCreated, nameLast = nameLast, nameFirst = nameFirst)
     *  }
     *  }}}
     */
    val RowParser : RowParser[M]

    /** SQL column list matching rowParser
     *
     *  Identifiers should already be quoted as necessary in this
     *  string.  A vertical bar can be included at the beginning of each
     *  line for use by stripMargin.
     *
     *  @example
     *  {{{
     *      override val RowParserColumns =
     *          """|    "Person"."id",
     *          |    "Person"."whenCreated",
     *          |    "Person"."nameLast",
     *          |    "Person"."nameFirst""""
     *  }}}
     */
    val RowParserColumns : String

    /** List of SQL column names for INSERT (without parentheses)
     */
    lazy protected val InsertColumnsSql = sqlInsertColumns(InsertColumns)

    /** List of SQL column value parameters for INSERT (without parentheses)
     *
     *  This is generated from `this.InsertColumns`.
     */
    lazy protected val InsertValueParametersSql = sqlInsertValueParameters(InsertColumns)
    // scalastyle:on field.name

    /* ============================================================== */
    /* Lists and individual identifiers and placeholders              */
    /* ============================================================== */

    /** Generate an SQL string of identifiers for an INSERT (without parentheses)
     *
     *  @param columns Columns
     *  @return        SQL INSERT string
     */
    def sqlInsertColumns(columns:Seq[String]) : String = {
        columns match {
            case Nil              => ""
            case cols:Seq[String] => cols.map(sqlQuoteIdentifier(_)).reduceLeft(_ + ListSep + _)
        }
    }

    /** Generate an SQL string for a single parameter placeholder
     *
     *  @param s Parameter
     *  @return  SQL parameter placeholder as a string
     */
    final def sqlParameterPlaceholder(s:String) : String = {
        "{" + sqlParameterName(s) + "}"
    }

    /** Generate an SQL string of value placeholders for an INSERT (without parentheses)
     *
     *  @param columns List of columns
     *  @return        SQL INSERT value string
     */
    def sqlInsertValueParameters(columns:Seq[String]) : String = {
        columns match {
            case Nil              => ""
            case cols:Seq[String] => cols.map(sqlParameterPlaceholder(_)).reduceLeft(_ + ListSep + _)
        }
    }

    /** Quote an SQL identifier
     *
     *  The identifier may be either a column name or table-name.column-name.
     *
     *  @param s Identifier to quote
     *  @return  Quoted identifier
     */
    def sqlQuoteIdentifier(s:String) : String = {
        s.split('.') match {
            case Array(table, column) => {
                s"$QuoteId${table.replaceAll(CharsToStripFromIdentifier, "")}$QuoteId.$QuoteId${column.replaceAll(CharsToStripFromIdentifier, "")}$QuoteId"
            }
            case Array(column)        => {
                s"$QuoteId${column.replaceAll(CharsToStripFromIdentifier, "")}$QuoteId"
            }
            case _                    => {
                // Invalid identifier
                s"$QuoteId$QuoteId"
            }
        }
    }

    /** Format an SQL parameter name
     *
     *  This removes invalid characters.  It does not enclose the string
     *  in braces.
     *
     *  @param s Parameter
     *  @return  SQL parameter name as a string
     */
    final def sqlParameterName(s:String) : String = {
        s.replaceAll(CharsToStripFromPlaceholder, "")
    }

    /* ============================================================== */
    /* SQL clauses                                                    */
    /* ============================================================== */

    /** Generate an SQL WHERE clause
     *
     *  `=`, `IN`, `AND`, `LIKE` are supported.  `<`, `>`, `OR` are not supported.
     *
     *  @param queryFilter Query filter to use in WHERE clause
     *  @return            (SQL whereClause, namedParameters)
     */
    def sqlWhereClause(queryFilter:Option[QueryFilter]) : (String, Seq[NamedParameter]) = {
        val conjunction = " AND "
        val emptyResult = ("", Seq.empty[NamedParameter])

        def filterToSql(filter:Tuple2[String, Seq[String]]) : (String, Seq[NamedParameter]) = {
            val name = filter._1
            val values = filter._2
            values match {
                case Nil    => emptyResult
                case Seq(a) => {
                    val sql = s"${sqlQuoteIdentifier(name)} " +
                        (if (a.contains(wildcard)) {
                            ("LIKE " + sqlParameterPlaceholder(name))
                        } else {
                            ("= " + sqlParameterPlaceholder(name))
                        })
                    val np = new NamedParameter(name = sqlParameterName(name), value = a.replace("%", "%%").replace(wildcard, "%"))
                    (sql, Seq(np))
                }
                case _      => {
                    val sql = s"${sqlQuoteIdentifier(name)} IN (${sqlParameterPlaceholder(name)})"
                    val np = new NamedParameter(name = sqlParameterName(name), value = values)
                    (sql, Seq(np))
                }
            }
        }

        (queryFilter match {
            case None                      => emptyResult
            case Some(QueryFilter(filter)) =>
                if (filter.isEmpty) {
                    emptyResult
                } else {
                    filter.map(filterToSql(_)).reduceLeft {
                        (b, a) => (b._1 + conjunction + a._1, a._2 ++ b._2)
                    }
                }
        }) match {
            case ("", nps:Seq[NamedParameter])       => ("", nps)
            case (s:String, nps:Seq[NamedParameter]) => ("WHERE " + s, nps)
        }
    }

    /** Generate an SQL ORDER BY clause
     *
     *  @param querySort Sort order
     *  @return      SQL ORDER BY clause
     */
    def sqlOrderByClause(querySort:Option[QuerySort]) : String = {
        def sortToSql(sort:Tuple2[String, Boolean]) : String = {
            sqlQuoteIdentifier(sort._1) + (if (sort._2) " DESC" else " ASC")
        }
        (querySort match {
            case None                 => ""
            case Some(sort:QuerySort) => sort.sort.map(sortToSql(_)).reduceLeft(_ + ListSep + _)
        }) match {
            case ""       => ""
            case s:String => "ORDER BY " + s
        }
    }

    /** Generate an SQL LIMIT clause
     *
     *  @param limit Number of rows for limit
     *  @return      SQL LIMIT clause
     */
    def sqlLimitClause(limit:Option[Int]) : String = {
        limit match {
            case (Some(l:Int)) if l > 0 => s"LIMIT $l"
            case _                      => ""
        }
    }

    /** Generate an SQL OFFSET clause
     *
     * @param offset Number of rows for offset
     * @return       SQL OFFSET clause
     */
    def sqlOffsetClause(offset:Option[Long]) : String = {
        offset match {
            case (Some(o:Long)) if o >= 0 => s"OFFSET $o"
            case _                        => ""
        }
    }

    /** Generate SQL LIMIT and OFFSET clauses
     *
     *  @param page Page
     *  @return     SQL LIMIT and OFFSET clauses
     */
    final def sqlLimitAndOffsetClauses(page:Option[QueryPage]) : (String, String) = {
        page match {
            case (Some(p:QueryPage)) => (sqlLimitClause(Some(p.size)), sqlOffsetClause(Some((p.number - 1) * p.size)))
            case _                   => ("", "")
        }
    }

    /* ============================================================== */
    /* CRUD operations                                                */
    /* ============================================================== */

    /** Delete records
     *
     *  All records are deleted from Table as specified by whereClause
     *  and namedParameters.
     *
     *  @param whereClause     SQL WHERE clause
     *  @param namedParameters Named parameters
     *  @param dbConnection    Database connection
     *  @return                A count of rows deleted, or an exception
     */
    protected def doDelete(whereClause:String, namedParameters:NamedParameter*)(implicit dbConnection:DbConnection) : Try[Int] = Try {
        SQL(s"""
            |DELETE
            |FROM ${sqlQuoteIdentifier(Table)}
            |WHERE
            $whereClause
        """.stripMargin.trim).
            asSimple().
            on(namedParameters:_*).
            executeUpdate()
    }

    /** Delete an M by ID
     *
     *  @param id           ID of the M record to delete
     *  @param dbConnection Database connection
     *  @return             A count of rows deleted, or an exception
     */
    def delete(id:Long)(implicit dbConnection:DbConnection) : Try[Int] = {
        Logger.debug(s"delete(id = $id) from $Table")

        val whereClause = s"""|    ${sqlQuoteIdentifier(Table + "." + PrimaryKey)} = {id}"""
        val namedParameter = "id" -> id
        doDelete(whereClause, namedParameter)
    }

    /** Delete an M by ID
     *
     *  @param id           ID of the M record to delete
     *  @param dbConnection Database connection
     *  @return             A count of rows deleted, or an exception
     */
    def delete(id:UUID)(implicit dbConnection:DbConnection) : Try[Int] = {
        Logger.debug(s"delete(id = $id) from $Table")

        val whereClause = s"""|    ${sqlQuoteIdentifier(Table + "." + PrimaryKey)} = {id}"""
        val namedParameter = "id" -> id
        doDelete(whereClause, namedParameter)
    }

    /** Find a single record
     *
     *  @param whereClause     SQL WHERE clause
     *  @param namedParameters Named parameters
     *  @param dbConnection    Database connection
     *  @return                optional M with the specified ID, or an exception
     */
    protected def doFind(whereClause:String, namedParameters:NamedParameter*)(implicit dbConnection:DbConnection) : Try[Option[M]] = Try {
        SQL(s"""
            |SELECT
            $RowParserColumns
            |FROM ${sqlQuoteIdentifier(Table)}
            |WHERE
            $whereClause
        """.stripMargin.trim).
            asSimple().
            on(namedParameters:_*).
            as(RowParser.singleOpt)
    }

    /** Find an M by ID
     *
     *  @param id           ID
     *  @param dbConnection Database connection
     *  @return             optional M with the specified ID, or an exception
     */
    def find(id:Int)(implicit dbConnection:DbConnection) : Try[Option[M]] = {
        Logger.debug(s"find(id = $id) in $Table")

        val whereClause = s"""|    ${sqlQuoteIdentifier(Table + "." + PrimaryKey)} = {id}"""
        doFind(whereClause, new NamedParameter(PrimaryKey, id))
    }

    /** Find an M by ID
     *
     *  @param id           ID
     *  @param dbConnection Database connection
     *  @return             optional M with the specified ID, or an exception
     */
    def find(id:Long)(implicit dbConnection:DbConnection) : Try[Option[M]] = {
        Logger.debug(s"find(id = $id) in $Table")

        val whereClause = s"""|    ${sqlQuoteIdentifier(Table + "." + PrimaryKey)} = {id}"""
        doFind(whereClause, new NamedParameter(PrimaryKey, id))
    }

    /** Find an M by ID
     *
     *  @param id           ID
     *  @param dbConnection Database connection
     *  @return             optional M with the specified ID, or an exception
     */
    def find(id:UUID)(implicit dbConnection:DbConnection) : Try[Option[M]] = {
        Logger.debug(s"find(id = $id) in $Table")

        val whereClause = s"""|    ${sqlQuoteIdentifier(Table + "." + PrimaryKey)} = {id}"""
        doFind(whereClause, new NamedParameter(PrimaryKey, id))
    }

    /** Insert an M
     *
     *  @param rsParser        Result set parser for the primary key
     *  @param namedParameters Named parameters
     *  @param dbConnection    Database connection
     *  @return                optional database-assigned ID of the new M, an an exception
     */
    protected def doInsert(rsParser:ResultSetParser[P], namedParameters:NamedParameter*)(implicit dbConnection:DbConnection) : Try[P] = Try {
        SQL(s"""
            |INSERT INTO ${sqlQuoteIdentifier(Table)}
            |    ($InsertColumnsSql)
            |VALUES
            |    ($InsertValueParametersSql)
        """.stripMargin.trim).
            asSimple().
            on(namedParameters:_*).
            executeInsert(rsParser)
    }

    /** Insert an M
     *
     *  @param m            Model object to insert
     *  @param dbConnection Database connection
     *  @return             optional database-assigned ID of the new M, an an exception
     */
    def insert(m:M)(implicit dbConnection:DbConnection) : Try[P] = {
        doInsert(PrimaryKeyResultSetParser, insertValues(m):_*)
    }

    /** Find a list of Ms
     *
     *  @param whereClause     SQL WHERE clause (including WHERE if not empty)
     *  @param orderByClause   SQL ORDER BY clause
     *  @param limitClause     SQL LIMIT clause
     *  @param offsetClause    SQL OFFSET clause
     *  @param namedParameters Named parameters
     *  @param dbConnection    Database connection
     *  @return                optional M with the specified ID
     */
    protected def doList(whereClause:String, orderByClause:String, limitClause:String, offsetClause:String, namedParameters:NamedParameter*)(implicit dbConnection:DbConnection) : Try[Seq[M]] = Try {
        Logger.debug(s"doList(whereClause = $whereClause, orderByClause = $orderByClause, limitClause = $limitClause, offsetClause = $offsetClause, namedParameters = $namedParameters) in $Table")

        SQL(s"""
            |SELECT
            $RowParserColumns
            |FROM ${sqlQuoteIdentifier(Table)}
            |$whereClause
            |$orderByClause
            |$limitClause
            |$offsetClause
        """.stripMargin.trim).
            asSimple().
            on(namedParameters:_*).
            as(RowParser.*)
    }

    /** Find a list of Ms
     *
     *  @param filter       filter
     *  @param page         page
     *  @param sort         sort
     *  @param dbConnection Database connection
     *  @return             optional M with the specified ID, or an exception
     */
    def list(filter:Option[QueryFilter] = None, page:Option[QueryPage] = None, sort:Option[QuerySort] = None)(implicit dbConnection:DbConnection) : Try[Seq[M]] = {
        Logger.debug(s"list(filter = $filter, page = $page, sort = $sort) in $Table")

        val (whereClause, namedParameters) = sqlWhereClause(filter)
        val orderByClause = sqlOrderByClause(sort)
        val (limitClause, offsetClause) = sqlLimitAndOffsetClauses(page)
        doList(whereClause = whereClause, orderByClause = orderByClause, limitClause = limitClause, offsetClause = offsetClause, namedParameters:_*)
    }
}
