/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.anorm

import java.util.UUID

import scala.util.{Failure, Success, Try}

import anorm.{Column, ToSql}
import org.postgresql.util.PGobject

/** Postgres database model
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @tparam M Model class
 *  @tparam P Primary key class
 */
abstract class ModelPostgres[M, P] extends Model[M, P] {
    override def companion : ModelPostgresCompanion[M, P]
}

/** Postgres companion object
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @tparam M Model class
 *  @tparam P Primary key class
 */
abstract class ModelPostgresCompanion[M, P] extends ModelCompanion[M, P] {
    // scalastyle:off field.name
    override implicit val UuidToSql : ToSql[UUID] = ToSql[UUID] {
        _ => ("?::uuid", 1)  // (SQL fragment, count of placeholders)
    }
    // scalastyle:on field.name

    /** Custom conversion from JDBC citext column to String
     *
     *  @see [[https://playframework.github.io/anorm/#column-parsers]]
     *
     *  @return An Anorm String column
     */
    implicit def columnToString : Column[String] = {
        Column.nonNull { (value, meta) =>
            // Extract the column name and JDBC class name from the column meta data.
            val anorm.MetaDataItem(column, nullable@_, clazz) = meta

            value match {
                case s:String                                  => Right(s)
                case pgo:PGobject if pgo.getType() == "citext" => Try { pgo.getValue() } match {
                    case Success(v) => Right(v)
                    case Failure(e) => Left(anorm.TypeDoesNotMatch(s"Error $column::$clazz value $value to String: ${e.getMessage}"))
                }
                case _                                         => Left(anorm.TypeDoesNotMatch(s"Cannot convert $column::$clazz value $value to String."))
            }
        }
    }
}
