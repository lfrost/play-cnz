/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play

/** Contains extensions to anorm.
 */
package object anorm
