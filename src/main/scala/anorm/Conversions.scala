/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.anorm

import java.sql.{PreparedStatement, Types => SqlTypes}
import java.time.{DayOfWeek, Month}

import anorm.{Column, MetaDataItem, ToStatement, TypeDoesNotMatch}

/** Database type conversions
 *
 *  @see [[https://playframework.github.io/anorm/#column-parsers]]
 *  @see [[https://playframework.github.io/anorm/#custom-parameter-conversions]]
 */
object Conversions {
    /** Custom conversion from JDBC column to DayOfWeek
     *
     *  Uses ISO 8601 day of week numbering (Monday = 1 to Sunday = 7).
     */
    implicit val ColumnIntToDayOfWeek : Column[DayOfWeek] = Column.nonNull { (value, meta) =>
        // Extract the column name and JDBC class name from the column meta data.
        val MetaDataItem(column, nullable@_, clazz) = meta

        value match {
            case i  : Int if (i >= 1 && i <= 7) => Right(DayOfWeek.of(i))
            case _                              => Left(TypeDoesNotMatch(s"Cannot convert $column::$clazz value $value to DayOfWeek."))
        }
    }

    /** Custom conversion to statement for type DayOfWeek
     *
     *  Uses ISO 8601 day of week numbering (Monday = 1 to Sunday = 7).
     */
    implicit val DayOfWeekToStatement : ToStatement[DayOfWeek] = new ToStatement[DayOfWeek] {
        override def set(statement:PreparedStatement, index:Int, value:DayOfWeek) : Unit = {
            if (value != null) {  // scalastyle:ignore null
                statement.setObject(index, value.getValue().toShort)
            } else {
                statement.setNull(index, SqlTypes.SMALLINT)
            }
        }
    }

    /** Custom conversion from JDBC column to Month
     *
     *  Month range 1 to 12.
     */
    implicit val ColumnIntToMonth : Column[Month] = Column.nonNull { (value, meta) =>
        // Extract the column name and JDBC class name from the column meta data.
        val MetaDataItem(column, nullable@_, clazz) = meta

        value match {
            case i  : Int if (i >= 1 && i <= 12) => Right(Month.of(i))
            case _                               => Left(TypeDoesNotMatch(s"Cannot convert $column::$clazz value $value to Month."))
        }
    }

    /** Custom conversion to statement for type Month
     *
     *  Month range 1 to 12.
     */
    implicit val MonthToStatement : ToStatement[Month] = new ToStatement[Month] {
        override def set(statement:PreparedStatement, index:Int, value:Month) : Unit = {
            if (value != null) {  // scalastyle:ignore null
                statement.setObject(index, value.getValue().toShort)
            } else {
                statement.setNull(index, SqlTypes.SMALLINT)
            }
        }
    }
}
