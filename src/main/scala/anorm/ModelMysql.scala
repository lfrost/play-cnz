/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.anorm

import java.util.UUID

import anorm.ToSql

/** MySQL database model
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @tparam M Model class
 *  @tparam P Primary key class
 */
abstract class ModelMysql[M, P] extends Model[M, P] {
    override def companion : ModelMysqlCompanion[M, P]
}

/** ModelMysql companion object
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @tparam M Model class
 *  @tparam P Primary key class
 */
abstract class ModelMysqlCompanion[M, P] extends ModelCompanion[M, P] {
    // scalastyle:off field.name
    override implicit val UuidToSql : ToSql[UUID] = ToSql[UUID] {
        _ => ("?", 1)  // (SQL fragment, count of placeholders)
    }
    override val QuoteId = '`'
    // scalastyle:on field.name
}
