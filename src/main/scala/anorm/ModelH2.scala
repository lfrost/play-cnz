/*
 * Copyright © 2017-2023 Lyle Frost <lfrost@cnz.com>
 */

package com.cnz.play.anorm

import java.util.UUID

import anorm.ToSql

/** H2 database model
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @tparam M Model class
 *  @tparam P Primary key class
 */
abstract class ModelH2[M, P] extends Model[M, P] {  // scalastyle:ignore class.name
    override def companion : ModelH2Companion[M, P]
}

/** ModelH2 companion object
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @tparam M Model class
 *  @tparam P Primary key class
 */
abstract class ModelH2Companion[M, P] extends ModelCompanion[M, P] {  // scalastyle:ignore class.name
    // scalastyle:off field.name
    override implicit val UuidToSql : ToSql[UUID] = ToSql[UUID] {
        _ => ("?", 1)  // (SQL fragment, count of placeholders)
    }
    // scalastyle:on field.name
}
